<?php
 /* ------------ C O N T R O L A D O R   F R O N T A L -------------*/
 //session_start(); 
 require_once __DIR__ . '/../app/Config.php'; 
 require_once __DIR__ . '/../app/Model.php';
 require_once __DIR__ . '/../app/Controller.php'; 

/* enrutamiento:  tienen marcadas las rutas de la petición  */

 $map = array( 	
	// 'registro' => array('controller' =>'Controller', 'action' =>'registro'),
     'inicio' => array('controller' =>'Controller', 'action' =>'inicio'),
     'valores' => array('controller' =>'Controller', 'action' =>'nosotros'),
     'contacto' => array('controller' =>'Controller', 'action' =>'contacto'),
     'generacion-de-leads' => array('controller' => 'Controller', 'action' =>'generacion_de_leads'),
     'telemarketing' => array('controller' => 'Controller', 'action' =>'telemarketing'),
     'remainder' => array('controller' => 'Controller', 'action' =>'remainder'),
     'experiencia-del-cliente'  => array('controller' => 'Controller', 'action' =>'experiencia_del_cliente'),
     'estudios-de-mercado'  => array('controller' => 'Controller', 'action' =>'estudios_de_mercado'),
     'tecnologia'  => array('controller' => 'Controller', 'action' =>'tecnologia'),
        'plataformas'  => array('controller' => 'Controller', 'action' =>'plataformas'),
        'acd'  => array('controller' => 'Controller', 'action' =>'acd'),
        'ivr'  => array('controller' => 'Controller', 'action' =>'ivr'),
        'marcador-predictivo'  => array('controller' => 'Controller', 'action' =>'marcador_predictivo'),
        'monitoreo-de-llamadas'  => array('controller' => 'Controller', 'action' =>'monitoreo_de_llamadas'),
        'grabacion-bajo-demanda'  => array('controller' => 'Controller', 'action' =>'grabacion_bajo_demanda'),        
        'integracion-de-telefonia-y-computo'  => array('controller' => 'Controller', 'action' =>'integracion_de_telefonia_y_computo'),
        'reportes'  => array('controller' => 'Controller', 'action' =>'reportes'),
        'mensajes-sms'  => array('controller' => 'Controller', 'action' =>'mensajes_sms'),
     'cobranza'  => array('controller' => 'Controller', 'action' =>'cobranza'),
     'capacitacion-y-mejora-continua' => array('controller' => 'Controller', 'action' => 'capacitacion_y_mejora_continua'),
     'backoffice' => array('controller' =>'Controller', 'action' =>'backoffice'),
     'bases' => array('controller' =>'Controller', 'action' =>'bases'),
     'galeria' => array('controller' =>'Controller', 'action' =>'galerias'),
     'comunidades' => array('controller' =>'Controller', 'action' =>'comunidades'),
     'consultoria-y-asesoria' => array('controller' =>'Controller', 'action' =>'consultoria_y_asesoria'),
     'atencion-al-cliente' => array('controller' =>'Controller', 'action' =>'atencion_al_cliente'),
     'listar' => array('controller' =>'Controller', 'action' =>'listar'),
     'insertar' => array('controller' =>'Controller', 'action' =>'insertar'),     
     'noticias' => array('controller' => 'Controller', 'action' => 'noticias'), /*****************************/
     'ver' => array('controller' =>'Controller', 'action' =>'ver'),
     'aportaciones' => array('controller' =>'Controller', 'action' =>'aportaciones'),
     'procesa-sugerencias' => array('controller' => 'Controller', 'action' => 'procesa_sugerencias'),
     'error' => array('controller' => 'Controller', 'action' => 'error'),
     'thankyou' => array('controller' => 'Controller', 'action' => 'thankyou'),
     
     'procesa-contacto' => array('controller' => 'Controller', 'action' => 'procesa_contacto'),         
     'exito' => array('controller' => 'Controller', 'action' => 'exito'),
     'admin' => array('controller' => 'Controller', 'action' =>'procesa_admin'),
     'administrador' => array('controller' => 'Controller', 'action' => 'sesion_cms'),
     'envia-datos' => array('controller' => 'Controller', 'action' => 'envia_datos'),
     'news' => array('controller' => 'Controller', 'action' => 'news'),    
     'noticias-detalle' => array('controller' => 'Controller', 'action' => 'noticias_detalle'),     
     'eliminaID' => array('controller' => 'Controller', 'action' => 'elimina_notice'),
     'xml' => array('controller' => 'Controller', 'action' => 'xml'),
 );
 
 // Parseo de la ruta para saber que acción disparar. Defino inicio como default
	 if (isset($_GET['ctl'])) {
		 if (isset($map[$_GET['ctl']])) {
			 $ruta = $_GET['ctl'];                                           
		 } else {
			 header('Status: 404 Not Found');
			 echo '<html><body><h1>Error 404: No existe la ruta <i>' .
					 $_GET['ctl'] .
					 '</p></body></html>';
			 exit;
		 }
	 } else {
		$ruta = 'inicio';
	 }
	 
 $controladora = $map[$ruta];
 
 
 // Ejecución del controlador asociado a la ruta. Muestra si la ruta esta definida en la tabla de rutas ($map)

 //if (isset($_SESSION['administrador'])) {
    //header('Location:http://localhost/prototipo_uno/app/administrador/layoutAdmin.php');
  //  call_user_func(array($controladora['controller'], $controladora['action']));
//}
//else {

 //   if (isset($_SESSION['ca_user']) && isset($_SESSION['ca_appname'])) {
/*
        if (method_exists($controladora['controller'], $controladora['action'])) {
            call_user_func(array($controladora['controller'], $controladora['action']));
            if (!isset($_GET['ctl'])) {
     //           session_destroy();
            }
        } else {
            header('Status: 404 Not Found');
            echo '<html><body><h1>Error 404: El controlador <i>' .
            $controlador['controller'] .
            '->' .
            $controlador['action'] .
            '</i> no existe</h1></body></html>';
        }
   // }
   //  else {
        //header('Location:http://localhost/prototipo_uno/web/sesion.php');
    }
}
 * 
 */
 
  if (method_exists($controladora['controller'],$controladora['action'])) {
           //	   echo array ( $controladora['controller'], $controladora['action']);
                  call_user_func( array ( $controladora['controller'], $controladora['action']) );
                  
                  /*
                  if (!isset($_GET['ctl'])) {                    
                    //session_destroy();
                    }*/
                  
            } else {

                header('Status: 404 Not Found');
                echo '<html><body><h1>Error 404: El controlador <i>' .
                        $controlador['controller'] .
                        '->' .
                        $controlador['action'] .
                        '</i> no existe</h1></body></html>';
            }            