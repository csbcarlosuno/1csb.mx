function LoginController($scope, $http)
{
    $scope.user = {};
    $scope.msg = {};

    $scope.loginAction = function(login)
    {
        
        $http({method: 'POST', url: 'class/Acces.php?User=' + login.email + '&Pass=' + login.password }).success(function(data)
        {
            $scope.user = $scope.posts = data; // response data 
            if ($scope.user.length != 0)
            {                                                                       
                location.href = "index.php?ctl=administrador";
            } else {
                $scope.msg = "El usuario o contraseña proporcionados son incorrectos.";
            }

        });
    }
}
