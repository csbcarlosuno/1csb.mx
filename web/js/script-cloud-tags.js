jQuery(function() {       
       if (!$('#myCanvas').tagcanvas({
            textColour: '#770505',
            outlineColour: '#ff00ff',
            reverse: true,
            depth: 0.8,
            interval: 5,
            maxSpeed:0.05,
            initial: [0.1,-0.1]
        }, 'tags'))
        {
            // something went wrong, hide the canvas container
            $('#myCanvasContainer').hide();
        }
});