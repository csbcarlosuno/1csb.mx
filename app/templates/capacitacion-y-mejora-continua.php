<?php ob_start(); ?>
<?php include 'functions/funciones.php'; ?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-8 alineacion">
            <h1 style="margin-bottom: 50px;">Capacitaci&oacute;n y mejora continua</h1>                
            <div class="caracteristicas-capacitacion-y-mejora-continua ">                
                <div>
                    <h4>Auditoria</h4>
                    <p>El objetivo de la auditoría es evaluar la suficiencia y efectividad de las disposiciones de calidad de nuestra organización mediante la recolección y uso de evidencia objetiva. <!--Además del registro sobre las instanancias de no cumplimiento con las disposiciones de calidad.--></p>
                </div>
                
                <div style="width: 700px;">
                    <h4>Capacitación de producto</h4>    
                    <ul class="camera_effected">
                        <li><strong>Concretamente, mediante la capacitación:</strong></li>
                        <li>Buscamos perfeccionar al colaborador en su puesto de trabajo.</li>   
                        <li>Todo en función de las necesidades de <span class="emphasis-2">UNO</span></li>
                        <li>Mediante un procesos estructurado definimos las metas</li>
                    
                    </ul>
                </div>
                
                <div>
                    <h4>Evaluaciones</h4>
                    <p> Proceso dinámico a través del cual, e indistintamente, <span class="emphasis-2">UNO</span> puede conocer sus propios rendimientos, especialmente sus logros y flaquezas, para así reorientar propuestas o bien focalizarse en aquellos resultados positivos para hacerlos aún más rendidores.</p>
                </div>    
            </div>  
            <!-- RESPONSIVE -->
            <div class="caracteristicas-capacitacion-y-mejora-continua-visible">
                <div>
                    <h4><strong>Auditoria</strong></h4>
                    <p>Maecenas leo ante, vehicula sagittis feugiat vel, adipiscing sit amet lectus. Fusce ullamcorper leo quis elit cursus interdum. Phasellus nisi libero, imperdiet a dictum in, pretium sed risus. </p>
                </div>
                
                <div>
                    <h4><strong>Capacitación de producto</strong></h4>    
                    <p>Nunc ac mi vel lacus pellentesque semper nec nec eros. Pellentesque rutrum massa et velit posuere interdum. In ullamcorper volutpat quam vitae pretium.</p>
                </div>
                
                <div>
                    <h4><strong>Evaluaciones</strong></h4>
                    <p> Aliquam tincidunt diam turpis, sit amet dictum ante varius a. Aenean porttitor ullamcorper metus vel dignissim. Cras nisl diam, tempus at neque ac, hendrerit ultricies ligula. Donec vel molestie dolor, nec congue lorem.</p>
                </div>    
            </div>
            <!-- END RESPONSIVE -->                    
        
        
        <div class="col-md-12 capacitacion">
            <img src="imgs/graficas-capacitacion-y-mejora-continua.png" class="img-responsive img-form-rapido" />
            <h2 class="header-form-adaptative" style="color:#000; text-align: left;">Objetivos:</h2>
            <p>Para <strong class="emphasis-2"> UNO </strong>parte importante para el cumpliminento de los objetivos son la CAPACITACIÓN, la EVALUACIÓN y la RETROALIMENTACIÓN, vizualizándolo como un proceso cíclico de mejora continua.</p> 
            <p> Dentro de <strong class="emphasis-2"> UNO CSB</strong> la capacitación otorga a nuestro staff las herramientas necesarias para la realizar sus funciones, así mismo brindamos seguimiento a las áreas de oportunidad detectadas dentro del procesos de auditoría y evaluación del servicio ofrecido, generando la herramienta necesaria para brindar una retroalimentación oportuna al staff y así lograr los estándares establecidos por nuestros socios de negocio. </p>
        </div>
        </div>
            
        <div class="col-md-4">            
           <!-- <h2 class="header-form-adaptative">Contácta con nosotros</h2>-->
            <?php formulario__rapido('Capacitación y mejora continua', 'block'); ?>
        </div>        
                
        <div class="col-md-4">
            <div style="background-color: #FFFBFA; border-radius: 10px; border:  #FADEDF solid 1px;margin-bottom: -20px; margin-top: 40px;" />   
                <img src="imgs/llamenos.png" class="img-responsive">
            </div>
        </div>
        
        <div class="col-md-4" style="margin-top: 67px; margin-left: 0px;">                                            
            <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> 
            <script src="js/jquery.lazylinepainter-1.4.1.min.js"></script>            
            <script src="js/script.lazy.line.painter.js" type="text/javascript"></script>

            <div id='btn'>
                <img src="imgs/frases-uno-contact-center-2.png" class="img-responsive" style="position: absolute; top: 14px; left: 40px;" />
            </div>             
        </div>
    </div>
    
</div>


<?php $contenido = ob_get_clean();?>
<?php include 'layout.php';?>

