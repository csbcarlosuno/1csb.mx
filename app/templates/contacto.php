<?php

ob_start();
?>
<script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?key=AIzaSyDkpgtrfgIUuKgh_mQ03DrYID6-pF5DkoI&sensor=false'></script>
<script type='text/javascript' src='js/validacion.js'></script>
<script type="text/javascript">
    function initialize() {
        var mapOptions = {
            center: new google.maps.LatLng(19.296907, -99.19175),
            zoom: 15,
            title: 'Ponte en contacto con nosotros, con gusto te atenderemos',
            icon: '/imgs/marcaMaps.png',
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        var contentString = '<div id="content" >' +
                '<div id="siteNotice">' +
                '</div>' +
                '<h3 id="firstHeading" class="mapsHeading">UNO Contact Solutions Bureaun (CSB)</h3>' +
                '<div id="mapsContent">' +
                '<p>Somos una empresa dedicada a proveer servicios de contacto a sus socios de negocios bajo diversos esquemas y soluciones, todas hechas a la medida de las necesidades de cada uno de ellos.' +
                'Somos UNO La prioridad UNO son nuestros clientes UNO nuestro lugar en el mercado.</p>' +
                '</div>' +
                '<img src="imgs/logo.png" class="img-responsive" style="float: right; width: 30%; margin-left: 100px;">' +
                '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 350
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(19.296957, -99.191743),
            map: map,
            title: 'UNO Contact Solutions Bureau'
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });


    }

</script>

<body onLoad="initialize()">

    <div class="jumbotron jumbotron-sm" style="padding-top: 10px; padding-bottom: 20px; background-color: #F4F4F4; margin-bottom:50px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h1 class="h1 icon-contact" style="font-size: 53px;">Cont&aacute;ctenos <small>No dude en contactar con nosotros</small></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8" style="padding-left: 0;">
                <div class="well well-sm" style="padding: 24px;">
                    <form method="post" action="index.php?ctl=procesa-contacto" name='Contacto' id="login">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nombre"><span class='obligatorio'>* &nbsp;</span> 
                                        Nombre:</label>
                                    <input type="text" class="form-control" name="name" id="nombre" placeholder="Ingresa tu nombre" required="required" />
                                </div>
                                <div class="form-group">
                                    <label for="name">
                                        Empresa:</label>
                                    <input type="text" class="form-control" name="company" id="name" placeholder="Empresa" required="required" />
                                </div>
                                <div class="form-group">
                                    <label for="email"><span class='obligatorio'>* &nbsp;</span> 
                                        Correo electr&oacute;nico:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                        </span>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Ingresa tu email" required="required" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone">
                                        Tel&eacute;fono:</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span>
                                        </span>
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Ingresa tu numero telefónico" required="required" /></div>
                                </div>
                                <div class="form-group">
                                    <label for="subject">
                                        Asunto:</label>
                                    <select id="subject" name="issue" class="form-control" required="required">
                                        <option value="na" selected="">Elige una opci&oacute;n:</option>
                                        <option value="Contratación de servicios">Contratación de servicios</option>
                                        <option value="Proveedores">Proveedores</option>
                                        <option value="Comunidades">Comunidades</option>
                                        <option value="Bolsa de trabajo">Bolsa de trabajo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="comentario"><span class='obligatorio'>*&nbsp;</span>
                                        Mensaje</label>
                                    <textarea name="message" id="comentario" class="form-control" rows="9" cols="25" required="required" placeholder="Ingresa un mensaje"></textarea> 
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="aviso" value="" id="privacy"><span class='obligatorio'>*</span> <a href=""  data-toggle="modal" data-toggle="modal" data-target=".bs-example-modal-lg">Aviso de Privacidad</a>                                                
                                            </label>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>                                                                                        

                            <input type='hidden' name='oculto' value='Pagina de contacto' />
                            <div class="col-md-3">
                                <input type="submit" onClick="enviarDatos();" class="btn btn-primary pull-right" id="btnContactUs" value='Env&iacute;a tu mensaje' />                                    
                            </div>
                            <div class="col-md-6 mensaje-obligatorio">Los campos con (<span class='obligatorio'> * </span>) son obligatorios</div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <form>
                    <legend style="border-bottom: none;"><span class="glyphicon glyphicon-globe"></span>Nuestra oficina</legend>
                    <address>
                        <strong>UNO ISB</strong><br>
                        Camino a Santa teresa 187C 2ndo piso<br>
                        Col. Parques del Pedregal, Delegación Tlalpan<br>
                        CP. 14010, México D.F. <br>                                                    

                        <p><abbr title="Phone" style="border-bottom: none">Telef&oacute;no:</abbr>1454 1201</p>

                    </address><!--
                    <address>
                        <strong>Envíanos un mail-.</strong><br>
                        <a href="mailto:cgutierrez@1csb.com.mx">first.last@example.com</a>
                    </address>-->
                </form>
            </div>
        </div>

        <div class="col-md-8" style="padding-left: 0;">
            <div id="map_canvas" style="width:100%; height:350px; margin-left: -16px;"></div>
            <img src='imgs/contacto-bottom.png' class='img-responsive' style="margin-left:-17px;" />
        </div>                
    </div>

</body>
<?php $contenido = ob_get_clean(); ?>


<?php include 'layout.php' ?>

