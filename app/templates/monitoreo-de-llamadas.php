<?php ob_start();?>
<?php include "functions/funciones.php"?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-9 alineacion">
            <img src="imgs/tecnologias/monitoreo-de-llamadas.jpg" class="img-responsive img-decorative" />
            <h2 class="encabezado-tecnologias">Monitoreo de llamadas</h2>
            <p><strong class="emphasis-2">Contact Center UNO</strong> ofrece las mejores soluciones en monitoreo de llamadas <!--y servicios profesionales relacionados desde 1998-->. Nuestra plataforma de grabación mejora el rendimiento del agente, aumenta las ventas, reducen la responsabilidad y ayudan a cumplir con los objetivos establecidos. Entre otras cuestiones es posible mejorar las áreas de oportunidad mediante el análisis de llamadas  y la gestión de resultados, así mismo buscamos mejorar la experiencia del cliente a eficientar el porcentaje de resoluciones en la primera llamada.</p>                                     
            <p>A través de una simple interfaz web es posible filtrar, buscar y reproducir, desde cualquier lugar, el audio de llamadas, e-mails, correos de voz y chats. Al disponer de un acceso al sistema de monitoreo de llamadas podrá visualizar la gestión que se esta llevando a cabo, siendo la transparencia con nuestros clientes ¡ uno de los principios más importantes !</p>            
            
            <ul class="camera_effected" style="margin-top: 40px; float:left; width: 62%; margin-right: 3%; padding-left: 0;">
                <li style="background: none;"><strong class="emphasis-2">Principales características:</strong></li>
                <li>Grabación en múltiples canales de Audio, siendo esta posibilidad la base para adaptarnos a cualquier entorno de trabajo</li>
                <li>Monitoreo de llamadas telefónicas en vivo y captura de pantalla, con detalles de la actividad del agente</li>
                <li>Generación de informes personalizados y mejora de las actividades de su negocio</li>
                <li>Reduce demandas legales asegurando el complimiento de regulaciones</li>               
                <li>Fácil recuperación y disposición de grabaciones telefónicas</li>
                <li>Mayor rendimiento de los agentes.</li>
                <li>Fácil acceso de sus grabaciones a través de una interfaz web</li>
                <li>Cifrado basado para asegurar todas las grabaciones y datos</li>                
            </ul>
            
            <div class="container-ancho-imagen-right" style="margin-top: 55px;">
                     <img src="imgs/tecnologias/monitoreo-de-llamadas-telefonicas.jpg" class="img-responsive img-decorative-sub" />
            </div>
            
            <div class="clearfix" ></div>
            <p style="margin-top: 50px;"><strong class="emphasis-2">Sabemos que hay muchas opciones cuando se trata de monitoreo y grabación de sus llamadas telefónicas, por ello personalizados y diseñamos un sistema que se adapta a todas sus necesidades actuales y futuras de la grabación.</strong></p>
        </div>
        
        <div class="col-md-3">
                    <!--<img src="imgs/btn-effect-form.jpg" class="img-responsive" id="pushButtomForm" style="cursor:pointer; cursor: hand;" />-->
                    <div class="form-quick" id="pushButtomForm"></div>

                    <?php  formulario__rapido('Monitoreo de llamadas', 'none'); ?>            
        </div>
        
        <?php barnav_tecnology('col-md-3') ?>
    </div>
</div>    

<script type="text/javascript">
    $(document).ready(function(){
        $("#pushButtomForm").click(function(){
        $(".showFormQuitly").slideToggle( "slow" );
        });
    });                             
</script>
<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>