<?php ob_start();?>
<?php include "functions/funciones.php"?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-9 alineacion">
            <img src="imgs/tecnologias/ivra.jpg" class="img-responsive img-decorative" />
            <div style="width: 62%; float: left; margin-top: 60px; margin-right: 3%">
            <p>A través del sistema de respuesta de voz interactiva proveemos de información a los clientes y cartera del contact center. Es la forma más adecuada para reducir los molestos tiempos de espera y las limitaciones de no tener un servicio las 24 horas. Mediante esta herramienta permitimos proveer de información e interacción con sus clientes de manera automática</p>
                           
            <p>Con el <strong class="emphasis-2">IVR</strong> integrado a un <strong class="emphasis-2">ACD</strong> optimizamos el trabajo de los agentes y facilitamos  el uso de canales de comunicación para integrar los datos y procesos que su organización necesita para asegurarse de la mejor productividad</p></p>
            
            <p>Por otra parte logramos personalizar el comportamiento del sistema según los requerimientos de su operación, por lo que podemos garantizar mejores y mayores rendimientos</p><br><br><br><br><br>
            
            
            </div>
            
            <div class="container-ancho-imagen-right" style="margin-top: 55px;">
                     <img src="imgs/tecnologias/ivraFuncionamiento.jpg" class="img-responsive img-decorative-sub" />
            </div>
            <div class='clearfix'></div>
                
        </div>
        
        
        <div class="col-md-3">       
            <div class="form-quick" id="pushButtomForm"></div>
            <?php  formulario__rapido('Respuesta de voz interactiva', 'none'); ?>            
        </div>
        
        <?php barnav_tecnology('col-md-3') ?>
    </div>   
</div>    

<script type="text/javascript">
    $(document).ready(function(){
        $("#pushButtomForm").click(function(){
        $(".showFormQuitly").slideToggle( "slow" );
        });
    });                             
</script>

<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>