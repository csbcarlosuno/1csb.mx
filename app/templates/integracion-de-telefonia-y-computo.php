<?php ob_start();?>
<?php include "functions/funciones.php"?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-9 alineacion">
            <img src="imgs/tecnologias/integracion-de-telefonia-y-computo.jpg" class="img-responsive img-decorative" />
            <h2 class="encabezado-tecnologias   ">Integración de telefonía y cómputo</h2>
            <p>En <strong class="emphasis-2">Contact Center UNO</strong> sabemos que la integración de la computadora con el teléfono no sólo es la industria del futuro, sino que está aquí y ahora para apoyar nuestros principios comerciales y seamos más pro-activos, y ofrezcamos al cliente un mejor servicio a un buen costo</p>                
            <p>Proveemos de ventajas competitivas a nuestros clientes porque combinamos la conveniencia del teléfono con la flexibilidad de la computadora. De esta manera, las características del teléfono se integran con la rapidez del acceso a la información mediante la pantalla de la computadora.</p>            
            <p>En forma práctica podemos visualizar las ventajas que ofrece la integración de la telefonía con el computo. En primer lugar el servicio al cliente mejorará considerablemente ya que el agente (al contar con este sistema integrado) contará con los elementos suficientes para asegurar al cliente el conocimiento de su situación. Por ejemplo: imaginemos que hacemos una llamada a un proveedor para obtener ayuda sobre un problema y la llamada es contestada con un saludo de bienvenida en el que escuchamos nuestro nombre. Por este simple hecho, la llamada tiene un buen comienzo. Además, si la persona que nos atiende demuestra conocer el producto y toda la información relevante sobre nosotros, nos sentiremos realmente confortables con la atención recibida.</p>
         
            <ul class="camera_effected" style="margin-top: 40px; float:right; width: 62%; margin-right: 3%; margin-right: 0;">
                <li style="background: none"><strong class="emphasis-2">Beneficios principales:</strong></li>
                <li>La forma de conectar la computadora personal con el teléfono es:</li>
                <li>Físicamente uniendo cada computadora a su respectivo teléfono (estándar TAPI),</li>
                <li>Estableciendo una conexión lógica entre la computadora y el teléfono (estándar TSAPI), uniendo en forma física exclusivamente al servidor de la red de computadoras PBX. </li>
                <li>La conexión física requiere de un nuevo hardware para cada enlace entre la computadora y el teléfono. Tales conexiones son establecidas con un módem o, más comúnmente, utilizando teléfonos digitales propietarios de los fabricantes del PBX. </li>
                <li>Por su parte, la conexión lógica este la computadora y el teléfono se realiza a través del software, aprovechando el enlace ya existente entre el servidor de la red de computadoras y el PBX. Con esto se logra una solución más flexible y económica que la conexión física. El beneficio será mayor mientras más alto sea el número de conexiones.</li>                
            </ul>
            
              <div class="container-ancho-imagen-left" style="margin-top: 55px; margin-right: 3%">
                     <img src="imgs/tecnologias/integracion-de-telefonia-y-computo-correcto.jpg" class="img-responsive img-decorative-sub" />
            </div>
            
            <div class="clearfix"></div>
            
            <div style="float:left; width: 59%;" class="encabezado-tecnologias-subtitulo">
                <p><strong class="emphasis-2">Aplicaciones del CTI</strong></p>

                <!--<p>El uso más común del CTI en los Centros Telefónicos es para el "screen pop", el cual permite que las llamadas sean transferidas a los representantes telefónicos de ventas o a los sistemas de IVR (Interactive Voice Response) y vayan acompañadas con datos relevantes del cliente. El beneficio más inmediato es la mejora en los niveles de servicio al cliente. Debido a que quien nos llama no será requerido con información repetitiva, minimizando así, el tiempo del representante telefónico de ventas en la llamada e incrementando su eficiencia.</p>-->
                <p>Otra característica del CTI es el "ruteo inteligente" (intelligent routing), en donde la aplicación reconoce el número que nos llamó (ya sea a través del ANI Automatic Number Identification o a través de la colecta de dígitos Prompting Digits). De esta manera, el script comienza con información relevante en la pantalla del representante telefónico de ventas o sistema de IVR (Interactive Voice Response). </p>
                <p>Adicionalmente, si se trata de una operación confidencial en la que se requiere una clave de seguridad, la llamada sería transferida nuevamente al sistema de IVR, libre de la intervención humana, para capturar y validar la clave en las bases de datos particulares y secretas del proveedor del servicio. Posteriormente es posible retornar la llamada al agente que inició la operación y concluirla agradablemente.</p>                        
            </div>     
            
            <div class="container-ancho-imagen-left" style="margin-top: 47px; margin-left: 6%">
                     <img src="imgs/tecnologias/integracion-de-telefonia-y-computo-funcionamiento.jpg" class="img-responsive img-decorative-sub" />
            </div>    <div class='clearfix'></div>                                                    
        </div>
        
        
        
        <div class="col-md-3">
            <!--<img src="imgs/btn-effect-form.jpg" class="img-responsive" id="pushButtomForm" style="cursor:pointer; cursor: hand;" />-->
            <div class="form-quick" id="pushButtomForm"></div>

            <?php  formulario__rapido('Integración de telefonía y computo', 'none'); ?>            
        </div>
        
        <?php barnav_tecnology('col-md-3') ?>
    </div>
</div>    

<script type="text/javascript">
    $(document).ready(function(){
        $("#pushButtomForm").click(function(){
        $(".showFormQuitly").slideToggle( "slow" );
        });
    });                             
</script>    
<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>