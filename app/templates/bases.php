<?php ob_start(); ?> 

<?php include 'functions/funciones.php';?>

<div class="container">
    
    <div class="col-md-12">
        <div class="col-md-8 alineacion">
            
            <h1>Bases de datos</h1>
            
            <p>El Banco de Datos lo vamos formando de acuerdo a compras o intercambio de datos que se realizan dentro del Contact Center, esto nos facilita la generación de una amplia gama de información que puede ser utilizada por el mismo Contact Center o por socios de negocio que así lo convengan.</p>
            <p>La inteligencia de Bases de Datos es un enfoque de <strong class="emphasis-2">UNO</strong> hacia el mejoramiento de prospectos que podemos acercar a las campañas, perfilando a los clientes para que se les puedan ofrecer los productos y/o servicios que realmente necesitan, buscando el mayor impacto en la productividad que de ahí se pudiera generar.</p>
            
            <img src="imgs/inteligencia-de-bases-de-datos-telemarketing-uno.png" class="img-responsive" />
            
            <img class="img-responsive" src="imgs/llama-ahora-uno-call-center.jpg" style="margin: 50px 0 0;" />
        </div>
    
    
        <div class="col-md-4">
              <!-- <h2 class="header-form-adaptative">Contácta con nosotros</h2>-->
               
            <?php                 
               formulario__rapido('Bases de datos', 'block'); 
            ?>             
            
        </div>
        
        <div class="col-md-4" style="padding: 0; margin-top: 50px; ">
            <div style="background-color: #FFFBFA; border-radius: 10px; border:  #FADEDF solid 1px; ">   
                <img src="imgs/llamenos.png" class="img-responsive" />
            </div>  
       </div>     
        
        <div class="col-md-4" style="margin-top: 47px;">                            
                
                <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> 
                <script src="js/jquery.lazylinepainter-1.4.1.min.js"></script>            
                <script src="js/script.lazy.line.painter.js" type="text/javascript"></script>
                
                <div id='btn'>
                    
                    <img src="imgs/frases-uno-contact-center-2.png" class="img-responsive" style="position: absolute; top: 14px; left: 40px;" />
                </div> 
            
         </div>
    </div>    
    
    
    
</div>

 <?php $contenido = ob_get_clean(); ?>

 <?php include 'layout.php' ?>