<?php ob_start(); ?>

<script type='text/javascript' src='libs/jquery.form.js'></script> 
<style type="text/css">    
 #aportaciones #success { /* hides form */
    position:absolute;
    top:0;left:0;
    width:100%;
    height:100%;
    border-radius: 10px;
    background-color: #FFFBFA;
    border: #FADEDF solid 1px;
}

#success p{
    text-align: center;
    margin-top: 40%;
    font-size: 26px;
}
</style>

<script type='text/javascript'>
    
$(document).ready(function() {
		
			$('#aportaciones').ajaxForm({
	//			beforeSubmit: enviarDatos,					  
				target: '#containerResponse',
				success: function() {												
						$('#containerResponse').fadeIn( 'slow' ); 
					}							
			});									   		
	});

function enviarDatos(){
	var f=document.Sugerencias;
                    
        if(f.comentario.value == 0){
		alert("Ingresa tu mensaje");
		f.comentario.value ="";
		f.comentario.focus();
		return false;
                 }
                 
                 f.submit();                       
                 return true;
}
</script>

<div class="container">        
    <div class="col-md-12">                        
        <img src='imgs/quejas-sugerencias.jpg' class='img-decorative img-responsive' name='Contacto' />
        <div class='col-md-4'>
            <div class='showFormQuitly'>
            <h1 class='titular_form_rapidos'>Comentarios y sugerencias</h1>    
            <form action='index.php?ctl=procesa-sugerencias' method='post' name='Sugerencias' id="aportaciones" style="position:relative;">
                <div class='formsRapidos'>

                    <div class='form-group'>
                        <label for='name'><span class='obligatorio'>(opcional)</span> 
                            Nombre:</label>
                        <input type='text' class='form-control' name='name' placeholder='Ingresa tu nombre'  />
                    </div>    

                    <div class='form-group'>
                        <label for='email'><span class='obligatorio'>(opcional)</span> 
                            Correo electr&oacute;nico:</label>
                        <div class='input-group'>
                            <span class='input-group-addon'><span class='glyphicon glyphicon-envelope'></span>
                            </span>
                            <input type='email' class='form-control' name='email' placeholder='Ingresa tu email' /></div>
                    </div>

                    <div class='form-group'>
                        <label for='name'> Mensaje:</label>
                        <textarea name='message' id='comentario' class='form-control' rows='5' cols='25'  placeholder='Mensaje'></textarea>                        
                    </div>    

                    <div class='col-md-12'>
                        <!--<input type='button' class='btn btn-primary pull-right' id='btnContactUs' onClick='enviarDatos();' />                            -->                        
                        <input type="submit" id="submit" value="Enviar" onClick="enviarDatos();" class="btn btn-primary pull-right" />
                    </div>
                    <div class="clearfix"></div>                    
                </div>
                <div id="containerResponse"></div>
            </form>
            </div>
        </div>
        
        <div class='col-md-8'>            
            
            <h4 style='margin: 110px 0 30px;'>A todo el personal de Uno Contact Center</h4>                     
            
            <p>Como Presidente del Comité de Auditoría de la Unión de Crédito y en
            cumplimiento a un acuerdo establecido, se ha dado de alta el correo
            electrónico <a href='mailto:transparencia@aaterracota.com.mx'>transparencia@aaterracota.com.mx</a>, 
            que yo personalmente tengo direccionado y solamente yo lo puedo abrir, al que todos los empleados de UNO Contact Solution Bureau pueden
            dirigirse cuando deseen manifestar o señalar algo de su entorno de trabajo, ya sea que provenga de la actuación, actitud o comportamiento
            presumiblemente inapropiados de algún compañero de trabajo o funcionario de un nivel superior, que a su parecer no represente una sana práctica
            empresarial o administrativa</p>
            
            <p>Por tanto, si tienen una queja o comentario de algo dentro de la empresa que no se esté dando dentro de los códigos de
            ética normalmente aplicables a la convivencia interna, no duden en enviar un comunicado al respecto.
            </p>
             
            <p><strong> Atte: René Solorzano </strong></p>
        </div>
        
    </div>
</div>    
 <?php $contenido = ob_get_clean() ?>
 <?php include 'layout.php' ?>
