<?php ob_start();?>
<?php include "functions/funciones.php"?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-9 alineacion">
            <img src="imgs/tecnologias/reportes.jpg" class="img-responsive img-decorative" />
            <h2>Generación de reportes</h2>
            <p>A través del sistema de generación de reportes de <strong class="emphasis-2">UNO CSB</strong> distribuye la información requerida a los socios que lo soliciten. Esta transmisión de información se puede efectuar mediante el movimiento físico de los elementos de almacenamiento o mediante la comunicación de señales digitales a dispositivos receptores (terminales, convertidores, estaciones remotas y otro computador).</p>                
            
            <div class="container-ancho-imagen-left">
                     <img src="imgs/tecnologias/reportes-graficas.jpg" class="img-responsive img-decorative-sub" />
            </div>

            <div style="float:right; width: 62%;">                
                
                 <ul class="camera_effected" style="float:left; margin-top: 30px;">
                    <li style="background: none; margin-bottom: 20px;"><strong class="emphasis-2">Los reportes que genera el sistema de información se clasifican en:</strong></li>
                    <li>Reporte de errores. los cuales proporcionan información sobre los errores que ocurren y se detectan durante el procesamiento de transacciones.</li>
                    <li>Reporte de actividad. proporciona información sobre las actividades o elementos de la organización.</li>
                </ul>                        
            </div><div class='clearfix'></div>                
        </div>
        
        
        
        <div class="col-md-3">
           <!-- <img src="imgs/btn-effect-form.jpg" class="img-responsive form-quick" id="pushButtomForm" style="cursor:pointer; cursor: hand;" />-->
            <div class="form-quick" id="pushButtomForm"></div>
            <?php  formulario__rapido('Reportes', 'none'); ?>            
        </div>
        <?php barnav_tecnology('col-md-3') ?>
    </div>
</div>    
<script type="text/javascript">
    $(document).ready(function(){
        $("#pushButtomForm").click(function(){
        $(".showFormQuitly").slideToggle( "slow" );
        });
    });                             
</script>    
<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>