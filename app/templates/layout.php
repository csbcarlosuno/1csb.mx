<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html lang="en">
     <head>
         <title>UNO Contact Solutions Bureau</title>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
         <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />         
         <meta http-equiv="X-UA-Compatible" content="IE=edge" /><!-- Etiqueta proporcionada por Bootstrap ???-->         
         <meta name="google-site-verification" content="UYLKjL8nekR48Wuj9uJPd8g5HF-ENzQTQ9v3HDx8Puk" />

         <link rel="stylesheet" href="dist/css/bootstrap.min.css" />         
         <link rel="stylesheet" id="camera-css"  href="css/camera.css" type="text/css" media="all" />         
         <link rel="shortcut icon" href="imgs/ico.jpg">
         <link rel="stylesheet" href="<?php echo 'css/' . Config::$mvc_vis_css; ?>" />
         
         <script src="https://code.jquery.com/jquery.js"></script> 
         <script src="dist/js/bootstrap.min.js"></script>
                
         <script type='text/javascript' src='js/correo-enviado.js'></script>
         
         <!--<script type='text/javascript' src='scripts/jquery.min.js'></script>-->
         <script type='text/javascript' src='scripts/jquery-1.7.2.min.js'></script>
         <!--<script type='text/javascript' src='http://code.jquery.com/jquery-1.10.2.min.js'></script>-->
         <!-- Desabilito esta opción porque no es posible ejecutar correctamente lazi line paiter-->
         <!--<script type='text/javascript' src='scripts/jquery.mobile.customized.min.js'></script>         -->
         <script type='text/javascript' src='libs/floatbox/framebox.js'></script>              
         <script type='text/javascript' src='scripts/jquery.easing.1.3.js'></script> 
         <script type='text/javascript' src='scripts/camera.min.js'></script>
         <script type='text/javascript' src='scripts/jquery.tagcanvas.min.js'></script>   
         <script type='text/javascript' src='js/script-cloud-tags.js'></script>
         <script type='text/javascript' src='js/menuOculto.js'></script>
         <script type='text/javascript' src='js/fixed-menu.js'></script>
                  
         
         
         <link rel='stylesheet' href='css/floatbox.css' />
         <script type='text/javascript' src='libs/marquee/jquery.marquee.min.js'></script>                  
         
         <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-42676133-1', '1csb.mx');
            ga('send', 'pageview');
         </script>

         
     </head>
     
     <body>

         <!--
         <div id='menuOculto' style="display: none;">
             <ul class="hide-device">
                 <li><a href='http://www.gimpsaelectrical.com/' title='GIMPSA EL&Eacute;CTRICAL'>Gimpsa electrical</a></li>
                 <li><a href='http://1csb.mx/' title='Call Center'>Gimpsa Corporativo</a></li>
                 <li><a href='http://www.prendal.com/' title='Casa de empe&ntilde;o'>Prendal</a></li>
             </ul>                
         </div>-->
                  
         <div class="containerMenu-oculto">
            <div class="logo"><a href="inicio"></a>
            <!--
                <script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
            <div id="SkypeButton_Call_emmanue.sanchez_1" style='position: absolute; top: 0px; right: 3%;'>
                <script type="text/javascript">
                  Skype.ui({
                    "name": "call",
                    "element": "SkypeButton_Call_emmanue.sanchez_1",
                    "participants": ["emmanue.sanchez"],
                    "imageSize": 32
                  });
                </script>
            </div>-->
                
                
            </div>            

            <!--
            <div class="menu-oculto">             
                    <a href="#" id="abrirMenuOculto" title="Contact Solutions Bureau"></a>             
            </div>
            -->
        </div>                                      
  
     <nav class="navbar navbar-default" role="navigation"  id='cabecera'>
         <div class="container-fluid">
             <!-- Conmutación de la navegación para dispositivos mobiles -->
             <div class="navbar-header">
                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                 </button>
                 <!--<a class="navbar-brand" href="#">Call center UNO</a>-->
             </div>

             <!-- Collect the nav links, forms, and other content for toggling -->
             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                 <ul class="nav navbar-nav">
                     <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Inicio<b class="caret"></b></a><span>Conócenos</span>
                         <ul class="dropdown-menu">                             
                             <li class="pagina-no-visible"><a href="galeria">Galer&iacute;a</a></li>
                         </ul>
                     </li>
                     <li class="dropdown">
                         <a href="valores">Nosotros<!--<b class="caret"></b>--></a><span>Qui&eacute;nes somos</span>                        
                     </li>
                     <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Servicios<b class="caret"></b></a><span>Lo mejor que hacemos</span>
                         <ul class="dropdown-menu">
                             <li class="dropdown-submenu">
                                 <a href="#">Call Center</a>
                                 <ul class="dropdown-menu">
                                     <li><a href="telemarketing">Telemarketing</a></li>
                                 </ul>
                             </li>
                             <li class="dropdown-submenu">
                                 <a href="#">Contact Center</a>
                                 <ul class="dropdown-menu">
                                     <li><a href="generacion-de-leads">Generación de leads</a></li>
                                     <li><a href="cobranza">Cobranza</a></li>
                                     <li><a href="atencion-a-clientes">Atenci&oacute;n al cliente</a></li>
                                    <li><a href="remainder">Remainder</a></li>
                                    <li><a href="experiencia-del-cliente">Experiencia del cliente</a></li>
                                    <li><a href="estudios-de-mercado">Estudios de mercado</a></li>
                                    <li><a href="capacitacion-y-mejora-continua">Capacitaci&oacute;n y mejora continua</a></li>                         
                                 </ul>
                             </li>
                             <li class="dropdown-submenu">
                                 <a href="#">Consultor&iacute;a</a>
                                 <ul class="dropdown-menu">
                                     <li><a href="consultoria-y-asesoria">Consultor&iacute;a y asesor&iacute;a</a></li>
                                     <li><a href="backoffice">Backoffice</a></li>
                                 </ul>
                             </li>                  
                         </ul>
                     </li><!--
                     <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Seguridad<b class="caret"></b></a><span>Subcontrataciones</span>
                         <ul class="dropdown-menu">
                             <li><a href="comunidades">Comunidades</a></li>
                         </ul>
                     </li>-->
                     <li><a href="comunidades">Comunidades</a><span>Subcontrataciones</span></li>
                     <li><a href="tecnologia">Tecnolog&iacute;a</a><span>Sistemas avanzados</span></li>
                     <li><a href="contacto">Contacto</a><span>Con gusto te atenderemos</span></li>
                     <li><a href="noticias-de-la-industria" >Noticias</a><span>Temas de inter&eacute;s</span></li>
                 </ul>
                 
             </div><!-- /.navbar-collapse -->
         </div><!-- /.container-fluid -->
     </nav>  

         
         <div class='clearfix'></div>    
     
      <div class="container-banner" role="main">             
             <?php echo $contenido ?>
      </div>

        <div class="icons-redes"><!--
            <a href="" class="icon-google"></a>    
            <a href="" class="icon-youtube"></a>-->
            <a href="https://www.facebook.com/pages/UNO-Contact-Solutions-Bureau/179060128853049" class="icon-face" target="_blank"></a>
            <a href="https://twitter.com/1_CSB" class="icon-twitter" target="_blank"></a>
            <a href="http://www.linkedin.com/company/uno-contact-solutions-bureau?trk=biz-companies-cym" class="icon-linkedin" target="_blank"></a>
        </div>
        <div class="clearfix"></div>


         <div id="pleca"></div>
         <div class="pie">
             
 <?php
/*
 function news() {
                    echo "<div class='feed marquee'>";
                    $long_descripcion = 100;
                    $num_noticias = 5;
                    $n = 0;
                    echo ("
                        <div class='conteinerNews'");
                            $noticias = simplexml_load_file('http://contactcenters.wordpress.com/feed/');
                            //$noticias = simplexml_load_file('http://1csb.mx/web/index.php?ctl=xml');
                            foreach ($noticias as $noticia) {
                                foreach ($noticia as $reg) {
                                    if ($reg->title != NULL && $reg->title != '' && $reg->description != NULL && $reg->description != '' && $n < $num_noticias) {
                                        echo "
                                                                <span class='feed'><a href='" . $reg->link . "' target='_blank' style='margin-top:10px;'>" . $reg->title . "</a></span>";
                                        $n++;
                                    }
                                }
                            }
                        echo "</div>";
                    echo "</div>";                                        
    }*/
    ?>
        -                <?php // news(); ?> 
                  
             <div class="feed marquee">
                 <div class="conteinerNews">
                     <span class='feed'>                         
                         <?php $notas= simplexml_load_file('xml/news.xml'); ?>
                         <?php foreach ($notas -> children() as $child  ) { ?>
                         <a href="index.php?ctl=noticias-detalle&id=<?php echo $child->id ?>" target="_blank" style="margin-top:10px;">
                             <?php                                                                                                                                                                                                                                                                                                                                                                                      
                                          echo $child->titulo ."&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;";                                          
                                     }                                                
                            ?>                                                          
                         </a>                       
                     </span>
                 </div>
             </div>
             
             <div class="ornamenta-vertical"></div>

                    <div class="container">                
                        <div class="col-md-6 acercadePie">
                            <h4 style="text-align: center; margin-top: 35px;">Somos una empresa de GIMPSA</h4>
                            <!--<p><strong class="emphasis-2">UNO Contact Solutions Bureau </strong>surge de la consolidación del <strong class="emphasis-2">Grupo IMPSA (GIMPSA)</strong>, que tras experimentar un proceso de expansión y crecimiento decide diversificar su presencia en el mercado nacional.</p>-->

                            <div class="grupo-impsa">
                                <a href="http://www.gimpsa.com.mx" class="gimpsa" target="_blank"></a>
                                <a href="#" title="UNO Custom Solutions Bureau" class="unoPie"></a>
                                <a href="http://www.gimpsaelectrical.com/" title="Gimpsa electrical" class="electrical" target="_blank"></a>
                                <a href="http://www.prendal.com/" title="Prendal Casa de empeño" target="_blank"></a>
                                <a href="http://www.credipyme.com.mx/" title="Credipyme" class='credipyme' target="_blank"></a>
                                <a href="http://www.stadiumplaza.com.mx/" title="Stadium Plaza" class='stadium' target="_blank"></a>
                            </div>
                        </div>                 

                        <div class="ornamenta-horizontal"></div>

                        <div class="col-md-2">
                            <ul class="lista-pie">
                                <li><a href="valores">Quiénes somos</a></li>
                                <li><a href="atencion-a-clientes">Atención a clientes</a></li>
                                <li><a href="tecnologia">Infraestructura</a></li>                                                                           
                            </ul>
                        </div>

                        <div class="col-md-2">
                            <ul class="lista-pie">
                                <li><a href="telemarketing">Telemarketing</a></li>
                                <li><a href="estudios-de-mercado">Estudios de mercado</a></li>
                                <li><a href="cobranza">Cobranza</a></li>
                            </ul>                         
                        </div>

                        <div class="col-md-2">
                            <ul class="lista-pie">                                                  
                                <li><a href="contacto">Contacto</a></li>                         
                                <li><a href=""  data-toggle="modal" data-toggle="modal" data-target=".bs-example-modal-lg" style="font-size: 11px; color: #CCC;">AVISO DE PRIVACIDAD</a></li>
                                <li><a href="sugerencias">Quejas y sugerencias</a></li>
                            </ul>
                        </div>
                    </div>
             
                    <p class="direccion">
                        Camino a Santa teresa 187C 2ndo piso Col. Parques del Pedregal, Tlalpan CP. 14010.  &nbsp; &nbsp;  | &nbsp; &nbsp;   Todos los derechos reservados. &copy;
                    </p>
         </div>
         
         <!-- Modal -->
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
           <div class="modal-dialog modal-lg">
             <div class="modal-content">

              <div class="modal-header">          
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><strong>Aviso de Privacidad</strong></h4>        
              </div>  

              <div class="modal-body">                

                  <p><span class="emphasis-2">Integrated Solutions Bureau S. de R.L. de C.V.</span> con domicilio en Camino a Santa Teresa # 187-C segundo piso, Col. Parques del Pedregal. Delegación Tlalpan. C.P. 14010 en México, D.F. es el responsable de recabar sus datos personales, del uso que se le de a los mismos y de su protección, siendo de suma importancia para Integrated Solutions Bureau S. de R.L. de C.V.  el resguardo y confidencialidad de sus datos personales, así como el uso responsable de ellos. Es por esto que en cumplimiento de la Ley  Federal de Protección de Datos Personales en Posesión de los Particulares (LFPDPP) hace de su conocimiento a:</p>
                <ul>
                    <li>Personas en proceso de reclutamiento o personas asignadas a desempeñar sus funciones en Integrated Solutions Bureau S. de R.L. de C.V. por sus patrones.</li>
                    <li>Proveedores.</li>
                    <li>Clientes y sus obligados por virtud de una negociación.</li>
                    <li>O cualquier otra persona de las que Integrated Solutions Bureau S. de R.L. de C.V. haya recabado datos./li>
                </ul>

                <p>Que la información personal recabada por Integrated Solutions Bureau S. de R.L. de C.V. y/o las empresas controladoras y/o las empresas filiales y/o subsidiarias y/o aquellos terceros que por la naturaleza de sus trabajos o funciones tengan la necesidad de tratar y/o utilizar sus datos personales, será utilizada para reclutamiento y selección de personal, capacitación y/o entrenamiento, brindar información sobre cambios en los servicios adquiridos, dar cumplimiento a las obligaciones contraídas con nuestros clientes,  coordinación de pagos y cobranza, evaluación de la calidad del servicio, realizar estudios e investigaciones de mercado, contactarlo por si requiere información adicional a nuestros servicios,  integrar bases de datos, enviarle boletines informativos y publicidad sobre eventos y productos,  así como para atender quejas y aclaraciones o  por su seguridad con el fin de autentificar su identidad.</p>
                <p>Para los fines antes mencionados la información la podrá recabar Integrated Solutions Bureau S. de R.L. de C.V. de manera enunciativa más no limitativa:</p>

                <ul>
                    <li>Nombre o nombre completo</li>
                    <li>Domicilio</li>
                    <li>Teléfono y/o celular</li>
                    <li>RFC o CURP</li>
                    <li>E-mail</li>
                    <li>Fecha de nacimiento,</li>
                    <li>Nacionalidad</li>
                    <li>Ocupación</li>
                    <li>Profesión</li>
                    <li>Actividad o giro al que se dedique la empresa o negocio</li>
                    <li>Información crediticia</li>
                    <li>Así como datos adicionales que se necesiten para el desarrollo de las actividades o servicios que se brindan.</li>
                </ul>

                <p>Usted tiene derecho de acceder, rectificar y cancelar sus datos personales así como de oponerse  al tratamiento de los mismos o revocar el consentimiento  que para tal fin nos haya otorgado a través de los procedimientos que hemos implementado (Derecho ARCO). Para conocer tales procedimientos, los requisitos y plazos ponemos a su disposición nuestra Oficina de Privacidad ubicada en Camino a Santa Teresa # 187-C segundo piso, Col. Parques del Pedregal. Delegación Tlalpan. C.P. 14010 en México, D.F. o al correo electrónico oficinadeprivacidad@1csb.com.mx con un horario de atención de lunes a viernes de 09:00 a 18:00 hrs.</p>
                <p>Los datos serán tratados de conformidad con la Ley Federal de Protección de Datos Personales en Posesión de  Particulares y su Reglamento.</p>

                <p>Se considera que autoriza el uso de sus datos personales conforme a este aviso de privacidad salvo nos comunique lo contrario.</p>

                <p> Integrated Solutions Bureau S. de R.L. de C.V. se compromete a no transferir su información personal a terceros sin su consentimiento salvo las excepciones previstas en el artículo 37 de la LFPDPPP, así como a realizar esta transferencia en los términos que fija la LFPDPPP. Sin embargo Integrated Solutions Bureau S. de R.L. de C.V.  podrá transferir sus Datos Personales a cualquiera  de las empresas  controladoras de esta última y/o sus empresas filiales y/o subsidiarias e incluso a terceras personas, nacionales o extranjeras, salvo que los titulares respectivos manifiesten expresamente su oposición en términos de lo dispuesto por la LFPDPPP.</p>

                <p>Si usted no manifiesta su oposición para que sus datos personales sean transferidos, se entenderá que  ha entregado su consentimiento para ello.</p>
                <p> Integrated Solutions Bureau S. de R.L. de C.V. se reserva el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente aviso de privacidad, para la atención de novedades legislativas, políticas internas o nuevos requerimientos para la prestación de servicios o productos.</p>

                <p>Si usted desea dejar de recibir mensajes promocionales de nuestra parte puede solicitarlo al correo electrónico: <a href="mailto:oficinadeprivacidad@1csb.com.mx?Subject=Aviso%20de%20privacidad"  target="_top">oficinadeprivacidad@1csb.com.mx</a></p>                                                                                                                                                                                                     
                <p>Estas modificaciones estarán disponibles al público a través  de nuestra página de internet  <a href="www.1csb.mx"> www.1csb.mx </a>en la sección de Aviso de Privacidad</a></p>
             </div>     

            </div>        
          </div>
        </div>   
     </body>
 </html>
