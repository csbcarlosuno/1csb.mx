<?php ob_start(); ?> 
<?php  include 'functions/funciones.php'; ?> 

<script type="text/javascript">
                    jQuery(function() {

        jQuery('#camera_wrap_1').camera({
            thumbnails: false,
            alignment: 'center',
            time: 2000,
            height: '35%'
        });

/*
        if (!$('#myCanvas').tagcanvas({
            textColour: '#ff0000',
            outlineColour: '#ff00ff',
            reverse: true,
            depth: 0.8,
            maxSpeed: 0.05
        }, 'tags'))
        {
            // something went wrong, hide the canvas container
            $('#myCanvasContainer').hide();
        }
*/        

    });
</script>


 
<div class="background-banner"></div>

   
     <div class="fluid_container">        
         <div class="camera_wrap camera_azure_skin" id="camera_wrap_1" >
             <div data-thumb="imgs/slides/thumbs/img1.jpg" data-src="imgs/slides/img1.png" data-fx="scrollLeft">
                 <div class="fadeIn camera_effected">                     
                     <h1>Consultor&iacute;a y asesor&iacute;a</h1>                     
                     <ul>
                         <li>Reingenier&iacute;a y desarrollo de procesos en call center</li>
                         <li>Operaci&oacute;n de Operaciones, tiempo, costo y alcance</li>
                         <li>Diseño, desarrollo e implementaci&oacute;n de software</li>
                         <li>Comunicaci&oacute;n con nuestros socios de negocios</li>
                     </ul>
                 </div>
                 <!--<div class="camera_caption fadeFromBottom">
                     Camera is a responsive/adaptive slideshow. <em>Try to resize the browser window</em>
                 </div>-->
             </div>
             <div data-thumb="imgs/slides/thumbs/img2.jpg" data-src="imgs/slides/img2.png" data-fx="scrollRight">
                 <!--<div class="camera_caption fadeFromBottom">
                     It uses a light version of jQuery mobile, <em>navigate the slides by swiping with your fingers</em>
                 </div>-->
                 <div class="fadeIn camera_effected">                     
                     <h1>Gesti&oacute;n de Cobranza</h1>  
                     <ul>
                         <li>Operamos tu cartera con estrategias y tecnolog&iacute;a de vanguardia<li>
                         <li>Gestionamos tu cartera con un grupo de profesionistas expertos</li>
                         <li>Disminuimos tus costos de operaci&oacute;n desde un 20 hasta un 50%</li>
                         <li>Maximizamos el alcance de la recuperación hasta en un 40%</li>
                     </ul>
                 </div>
             </div>
             <div data-thumb="imgs/slides/thumbs/img3.png" data-src="imgs/slides/img3.png" data-fx="scrollHorz">
                 <!--<div class="camera_caption fadeFromBottom">                     
                     <em>It's completely free</em> (even if a donation is appreciated)
                 </div>-->
                 <div class="fadeIn camera_effected">                     
                     <h1>Atenci&oacute;n a clientes</h1>                     
                     <ul>
                         <li>Servicio a clientes<li>
                         <li>Retenci&oacute;n y recuperaci&oacute;n de clientes</li>
                         <li>Programas de lealtad y log&iacute;stica</li>
                         <li>Implementamos operaciones garantizando el cumplimiento de los procesos desarrollados por nuestros socios.</li>
                     </ul>
                 </div>
             </div>
             <div data-thumb="imgs/slides/thumbs/img4.png" data-src="imgs/slides/img4.png" data-fx="scrollBottom">
                 <!--<div class="camera_caption fadeFromBottom">
                     Camera slideshow provides many options <em>to customize your project</em> as more as possible
                 </div>-->
                 <div class="fadeIn camera_effected">                     
                     <h1>Telemarketing</h1>                     
                     <ul>
                         <li>Atenci&oacute;n de campañas con llamadas de entrada (Inbound) <li>
                         <li>Equipos especializados y orientados en generar prospectaci&oacute;n y venta a trav&eacute;s de llamadas de salida (Outbound).</li>
                         <li>Herramientas enfocadas a reforzar y optimizar los resultados de nuestros clientes</li>                         
                     </ul>
                 </div>
             </div>
             <div data-thumb="imgs/slides/thumbs/img5.png" data-src="imgs/slides/img5.png" data-fx="scrollTop">
                 <!--<div class="camera_caption fadeFromBottom">
                     It supports captions, HTML elements and videos and <em>it's validated in HTML5</em> (<a href="http://validator.w3.org/check?uri=http%3A%2F%2Fwww.pixedelic.com%2Fplugins%2Fcamera%2F&amp;charset=%28detect+automatically%29&amp;doctype=Inline&amp;group=0&amp;user-agent=W3C_Validator%2F1.2" target="_blank">have a look</a>)
                 </div>-->
                 <div class="fadeIn camera_effected">                     
                     <h1>Estudios de mercado</h1>                     
                     <ul>
                         <li>Satisfacci&oacute;n de clientes.<li>
                         <li>Comportamiento del mercado.</li>
                         <li>Consulta de avances en l&iacute;nea.</li>
                         <li>Alternativas de monitoreo remoto</li>
                         <li>Contamos con la capacidad para el desarrollo y montaje de cuestionarios</li>
                     </ul>
                 </div>
             </div>
             <div data-thumb="imgs/slides/thumbs/img6.png" data-src="imgs/slides/img6.png" data-fx="scrollTop">
                 <!--<div class="camera_caption fadeFromBottom">
                     It supports captions, HTML elements and videos and <em>it's validated in HTML5</em> (<a href="http://validator.w3.org/check?uri=http%3A%2F%2Fwww.pixedelic.com%2Fplugins%2Fcamera%2F&amp;charset=%28detect+automatically%29&amp;doctype=Inline&amp;group=0&amp;user-agent=W3C_Validator%2F1.2" target="_blank">have a look</a>)
                 </div>-->
                 <div class="fadeIn camera_effected">                     
                     <h1>Comunidades (Subcontrataciones)</h1>                     
                     <ul>
                         <li>Trabajo en conjunto con campañas de telemarketing, in bound y out bound</li>
                         <li>Capacitaciones para todo tipo de campaña</li>
                         <li>Desarrollo de sistemas según las necesidades</li>
                         <li>Objetivos que están orientados a ganar - ganar</li>                         
                     </ul>
                 </div>
             </div>
         </div>
     </div>    

    <!--Carousel accesible desde dispositivos móbiles -->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
              <li data-target="#carousel-example-generic" data-slide-to="3"></li>
              <li data-target="#carousel-example-generic" data-slide-to="4 "></li>
            </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="imgs/slides/img1.jpg" alt="Consultoría y asesoría">
                <div class="carousel-caption"></div>
            </div>

            <div class="item">
                <img src="imgs/slides/img2.jpg" alt="Gestión de cobranza">
                <div class="carousel-caption"></div>
            </div>
            
            <div class="item">
                <img src="imgs/slides/img3.jpg" alt="Atención a clientes">
                <div class="carousel-caption"></div>
            </div>
            
            <div class="item">
                <img src="imgs/slides/img4.jpg" alt="Telemarketing">
                <div class="carousel-caption"></div>
            </div>
            
            <div class="item">
                <img src="imgs/slides/img5.jpg" alt="Estudios de mercado">
                <div class="carousel-caption"></div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>    
    <!-- End Carousel para dispositivos móbiles -->


<div class="container">
    <div class="clearfix" style="margin-bottom: 50px;"></div>
    <div class="row show-grid" >
        <div class="col-md-4" style="padding-top: 30px">
            <h1>¿Quiénes somos?</h1>
            
            
            <p><span class="emphasis-2">UNO Contact Solutions Bureau</span> somos un Contact Center dedicado a generar alianzas estratégicas que nos permitan desarrollar modelos y soluciones de negocio que satisfagan las necesidades de nuestros socios de negocio. </p>
            <p>Con operaciones desde el 2008, <strong class="emphasis-2"> UNO Contact Solutions Bureau </strong>es una marca registrada de <strong class="emphasis-2"> ISB (Integrated Solutions Bureau S de RL) </strong> formando parte de <strong class="emphasis-2"> Grupo GIMPSA </strong> </p>            
        </div>    
       
        <?php 
            barnav_tecnology('col-md-4');
        ?>
        
        <?php       
        cloud_tags();
        ?>

    </div>        
</div>



 <?php $contenido = ob_get_clean(); ?>

 <?php include 'layout.php' ?>