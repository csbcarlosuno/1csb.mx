<?php ob_start();?>
<?php include "functions/funciones.php"?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-9">        
            <img src="imgs/tecnologias/acd.jpg" class="img-responsive img-decorative" />            
            <div class="col-md-9 alineacion">                        
                <h2  class="encabezado-tecnologias" style="margin-bottom: 20px;">Distribuidor automático de llamadas</h2>
                <p>Con el <strong class="emphasis-2">Distribuidor automático de llamadas</strong> (ACD) incrementamos la productividad de cualquier servicio. Brindamos una mejor experiencia a sus clientes si distribuimos de forma adecuada las llamadas, correos electrónicos, chats, solicitudes de call-back y correos de voz en forma inteligente a los agentes mejor calificados  para atenderlos. </p>
            </div>
            <div class="col-md-3" style="margin-bottom: 40px;"><img src="imgs/tecnologias/distribuidor-automatico-de-llamadas.png" class="img-responsive" /></div>
            <div class="clearfix"></div>

                <div class='col-md-3'>
                <img src="imgs/tecnologias/distribuidor-automatico-de-llamadas-aprobacion.png" class="img-responsive" />
            </div>
            <div class="col-md-9">            
                <p>Los clientes y socios prefieren un rápido y atento servicio porque les asegura una mayor confianza, calidad y una mayor certeza. Al decidir guiarse por estas alternativas estará dando a sus clientes y/o socios lo más sofisticado y apropiado para hacerse de mayores y mejores mercados</p>
                <p>Por otro lado con estos sistemas permitimos que fácilmente, y desde una interfaz web, se personalice y automatice el proceso de distribución de los contactos con sus clientes en forma eficiente  a la vez transparente</p>

            </div>
            <div class="clearfix"></div>
                <ul class="camera_effected encabezado-tecnologias-subtitulo">
                    <li style="background: none;"><strong class="emphasis-2">Funcionalidades:</strong>
                    <li>Manejo de interacciones de chat, e-mail, fax y llamadas</li>
                    <li>Un único ACD para todas las interacciones</li>
                    <li>Múltiples criterios de distribución de llamadas</li>
                    <li>Múltiples colas y grupos de agentes</li>
                    <li>Ruteo de interacciones basado en habilidades</li>
                    <li>Mensajes o música de espera customizados según número llamado o campaña</li>
                    <li>Aviso de lugar y tiempo de espera en cola</li>
                    <li>Transferencia y conferencia</li>
                </ul>        

        </div>                    

        <div class="col-md-3">        
            <div class="form-quick" id="pushButtomForm"></div>        
            <?php  formulario__rapido('Distribuidor automático de llamadas', 'none'); ?>            
        </div>

        <?php barnav_tecnology('col-md-3') ?>
    </div>
</div>   

<script type="text/javascript">
    $(document).ready(function(){
        $("#pushButtomForm").click(function(){
        $(".showFormQuitly").slideToggle( "slow" );
        });
    });                             
</script>
<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>