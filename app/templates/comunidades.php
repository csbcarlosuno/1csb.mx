 <?php ob_start(); ?> 
<?php include 'functions/funciones.php'; ?>

<div class="container">
    
    
    <div class="col-md-12">
        <div class="col-md-8 alineacion">
        <h1>Comunidades</h1>
        
        <p>
            Los éxitos que en <strong class="emphasis-2">UNO CSB </strong> hemos alcanzado son gracias al fuerte equipo que nos respalda, que se compone de personal altamente especializado y una innovadora tecnología, que nos permite acercar a nuestros socios atractivas Propuestas Comerciales con oportunidad de trabajar con nuestras campañas de telemarketing, in bound, out bound, etc. 
        </p>
    
        <p>
            Brind&aacute;ndoles el respaldo necesario para la implementación de las campañas, que abarca desde capacitaciones de la campaña hasta el desarrollo del mejor sistema que se adapte a sus necesidades. Todo esto para llegar al alcance de nuestros objetivos que están orientados a un ganar - ganar.
        </p>
        
        <img class="img-responsive" src="imgs/comunidades-uno-contact-solutions-bureau.jpg"  />
        <img class="img-responsive" src="imgs/llama-ahora-uno-call-center.jpg" />
        </div>
        
        <div class="col-md-4">
            
            <!--<h2 class="header-form-adaptative">Contácta con nosotros</h2>-->
            <?php            
            formulario__rapido('Comunidades', 'block');
            ?>
            
            <div class='container-imgs-frases'>
                <img src='imgs/frases-uno-contact-center.png' class='img-responsive' /> 
            </div>
            
        </div>
    </div>

<?php $contenido = ob_get_clean(); ?>
 <?php include 'layout.php' ?>