<?php ob_start(); ?>
<?php include 'functions/funciones.php'; ?>

<div class="container">
    <div class="col-md-12">       
        
        <div class="col-md-8 alineacion" style="margin-top:40px;">
            <img src="imgs/cobranza-imagen.png" class="img-responsive" />            
                
                <h2 class="alineacion2">¿Qué es la gestión de cobranza?</h2>
                <p>En <strong class="emphasis-2">UNO CSB Contact Center</strong> desarrollamos modelos comerciales acorde al perfil de las carteras operándolas por medio de estrategias específicas y personalizas de acuerdo al cliente y sus necesidades gracias al grupo de profesionistas expertos que se encargan de llevar a cabo el proceso de cobro.</p>
                <p>Como gestores de Cobranza nuestra acciones están encaminadas para facilitar  la recuperación de  créditos, de manera que los activos exigibles de la institución se conviertan en activos líquidos de la manera más rápida y eficiente posible por lo que resulta primordial mantener una buena relación con los deudores y su disposición en futuras negociaciones no se vea afectada.</p>                
                
                <ul class="camera_effected" style="margin: 30px 0;">
                    <li style="background: none"><strong>La gestión de cobranza permite:</strong></li>
                    <li>Recuperación de Cartera Fallida</li>
                    <li>Reducir la tasa de morosidad</li>
                    <li>Garantizar la Supervivencia de la Institución</li>
                    <li>Brindar un mejor servicio</li>
                </ul>                                
                
                <p>Por otra parte la gestión de cobranza es un proceso que parte del análisis de la situación del cliente, seguido de un frecuente y oportuno contacto. En este sentido <strong class="emphasis-2"> UNO CSB</strong> le ofrece alternativas de solución oportunas dependiendo el caso y en determinado caso realizar un seguimiento continuo sobre el control del cumplimiento de los acuerdos negociados. </p>
                <!--
                <ul class="camera_effected">
                        <li style="background:none; font-size: 16px; margin-top: 20px;"><strong>Tipos de cobranza</strong></li>
                        <li style="background:none">Preventiva</li>                            
                        <li>Asuntos en los que no se exceda la fecha límite de pago.</li>                            
                        <li style="background:none">Administrativa</li>                            
                        <li>Morosidad de la cartera en 30, 60 o 90 días, antes de write off.</li>                            
                        <li style="background:none">Extrajudicial</li>                                                           
                        <li>Morosidad de la cartera superior a 90 días.</li>                        
                        <li style="background:none">Prelegal</li>                            
                        <li>Morosidad de la cartera superior a 180 días.</li>                            
                        <li style="background:none">Legal</li>                            
                        <li>Recuperación de los adeudos a través de juicios.</li>                        
                    </ul>
                -->
                <h2 class="alineacion2">Nuestro CRM está diseñado de la siguiente manera:</h2>
                
               <div class="col-md-12" style="padding:0;">
                    <img src="imgs/cobranza-proceso.jpg" class="img-responsive img-decorative-sub" />
                </div>
                                
                <div class="clearfix"></div>                                     
                
                <div class="col-md-12">
                    <ul class="camera_effected">
                        <li style="background:none"><strong>Ventajas de contratar los servicios de un UNO CSB:</strong></li>
                        <li>Personal altamente capacitado y especializado</li>
                        <li>Disminución de costos y tiempo, ya que toda la operación y resultados los realiza <strong class="emphasis-2">UNO CSB </strong> a través del control y supervisión constante.</li>
                        <li>El deudor es sorprendido al ser contactado por una empresa diferente y de nuevos gestores.</li>
                        <li>Mayor capacidad para crear estrategias a recuperar las cuentas perdidas.</li>
                    </ul>
                </div>                
    </div>               
        
        <div class="col-md-4">            
             <!--<h2 class="header-form-adaptative">Contácta con nosotros</h2>-->
            <?php formulario__rapido('cobranza', 'block'); ?>
        </div>
        
        <div class="col-md-4">
            <div style="background-color: #FFFBFA; border-radius: 10px; border:  #FADEDF solid 1px;margin-bottom: -20px; margin-top: 40px;" />   
                <img src="imgs/llamenos.png" class="img-responsive">
            </div>
        </div>
        
        <div class="nube-etiquetas-no-visible">
        <?php  cloud_tags();  ?>
        </div>
        
        <div class="col-md-4" style="margin-top: 67px; margin-left: 0px;">                                            
            <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> 
            <script src="js/jquery.lazylinepainter-1.4.1.min.js"></script>            
            <script src="js/script.lazy.line.painter.js" type="text/javascript"></script>

            <div id='btn'>
                <img src="imgs/frases-uno-contact-center-2.png" class="img-responsive" style="position: absolute; top: 14px; left: 40px;" />
            </div>             
        </div>
        
</div>    
</div>

<?php $contenido = ob_get_clean();?>
<?php include 'layout.php';?>
