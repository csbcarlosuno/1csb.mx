<?php ob_start();?>
<?php include "functions/funciones.php"?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-9 alineacion">
            <img src="imgs/tecnologias/marcador-predictivo.jpg" class="img-responsive img-decorative" />
            <h2 class="encabezado-tecnologias">Marcador predictivo</h2>             
            <p>Un Marcador Predictivo es un sistema automatizado y computarizado (hardware y software) que marca los lotes de números de teléfono y, a continuación, una vez que la otra persona ha respondido, transmite las llamadas a un operador asignado a las ventas u otras campañas.</p>
                
            <p>Los marcadores predictivos pueden aumentar considerablemente el número de contactos de voz humana que un agente puede hacer en un día de trabajo.</p>
            
            <p>Un marcador predictivo lleva a cabo muchas tareas. En la base de un sistema de marcador predictivo es un administrador de listas que organiza demográficamente las listas de teléfono en grupos relacionados y transfiere estos números de teléfono al marcador.</p>
            
            <ul class="camera_effected" style="margin-top: 40px; float:left; width: 65%;">
                <li style="background: none; margin-bottom: 20px;"><strong class="emphasis-2">El marcador predictivo es utilizado para:</strong></li>
                <li>Telemarketing (ventas BtoC y BtoB, gestión de citas, generación de clientes);</li>
                <li>Investigación de mercado;</li>
                <li>Relaciones públicas y colecciones;</li>
                <li>Sondeos de opinión</li>
                <li>Encuestas</li>
                <li>Servicio de atención al cliente: Seguimiento</li>
                <li>Caridades;</li>
                <li>Recordatorios;</li>
                <li>Notificaciones.</li>
            </ul>
                     
            
            <div class="container-ancho-imagen-right" style="margin-top: 55px;">
                     <img src="imgs/tecnologias/marcador-predictivo-exito-agentes.jpg" class="img-responsive img-decorative-sub" />
            </div>
            <div class="clearfix"></div>
        </div>
        
        
        
        <div class="col-md-3">
        <!--<img src="imgs/btn-effect-form.jpg" class="img-responsive" id="pushButtomForm" style="cursor:pointer; cursor: hand;" />-->
            <div class="form-quick" id="pushButtomForm"></div>

            <?php  formulario__rapido('Marcador predictivo', 'none'); ?>            
        </div>
        
        <?php barnav_tecnology('col-md-3') ?>
    </div>
</div>    

<script type="text/javascript">
    $(document).ready(function(){
        $("#pushButtomForm").click(function(){
        $(".showFormQuitly").slideToggle( "slow" );
        });
    });                             
</script>

<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>