<?php 

    function formulario__rapido($valor, $style)
    {
        echo("
            <!-- FORMULARIOS RÁPIDOS -->
            <script type='text/javascript' src='js/validacion.js'></script>
            <div class='showFormQuitly' style='display: ".$style."'>
            <h1 class='titular_form_rapidos'>CONTÁCTANOS</h1>    
            <form action='index.php?ctl=procesa-contacto' method='post' name='Contacto'>
                <div class='formsRapidos'>

                    <div class='form-group'>
                        <label for='name'><span class='obligatorio'>* &nbsp;</span> 
                            Nombre:</label>
                        <input type='text' class='form-control' name='name' id='nombre' placeholder='Ingresa tu nombre' required />
                    </div>    

                    <div class='form-group'>
                        <label for='email'><span class='obligatorio'>* &nbsp;</span> 
                            Correo electr&oacute;nico:</label>
                        <div class='input-group'>
                            <span class='input-group-addon'><span class='glyphicon glyphicon-envelope'></span>
                            </span>
                            <input type='email' class='form-control' name='email' id='email' placeholder='Ingresa tu email' required='required' /></div>
                    </div>

                    <div class='form-group'>
                        <label for='name'><span class='obligatorio'>* &nbsp;</span> Mensaje</label>
                        <textarea name='message' name='message' id='comentario' class='form-control' rows='5' cols='25' required='required' placeholder='Mensaje'></textarea>
                        <input type='hidden' name='oculto' value='".$valor."' />
                    </div>    

                    <div class='col-md-12'>
                        <button type='buttom' class='btn btn-primary pull-right' id='btnContactUs' onClick=' enviarDatos();'>
                            Env&iacute;a tu mensaje</button>
                    </div>
                </div>
            </form>
            </div>
            
        <!-- END -->
        ");
    }
    
    
    function formulario__rapido_leads_generation(){
        echo("
                <div class='row formsRapidos-leads-genration' style='margin-left: 30px; margin-right: 30px'>

                    <div class='form-group'>
                        <label for='name'>
                            Nombre:</label>
                        <input type='text' class='form-control' id='name' placeholder='Ingresa tu nombre' required />
                    </div>    

                    <div class='form-group'>
                        <label for='email'>
                            Correo electr&oacute;nico:</label>
                        <div class='input-group'>
                            <span class='input-group-addon'><span class='glyphicon glyphicon-envelope'></span>
                            </span>
                            <input type='email' class='form-control' id='email' placeholder='Ingresa tu email' required='required' /></div>
                    </div>

                    <div class='form-group'>
                        <label for='name'>Mensaje</label>
                        <textarea name='message' id='message' class='form-control' rows='5' cols='25' required='required' placeholder='Mensaje'></textarea>
                    </div>    

                    <div class='col-md-12'>
                        <button type='submit' class='btn btn-primary pull-right' id='btnContactUs'>
                            Env&iacute;a tu mensaje</button>
                    </div>
                </div>
        ");
    }
    
    function barnav_tecnology($param){
        echo("
            
            <div class='". $param ." clearfix' style='padding-top: 30px'>
           <h3 class='barNav-tecnology'>Tecnolog&iacute;a</h3>
           <ul class='style-vineta'>
               <li><a href='plataformas' title='Plataformas tecnológicas'>Plataformas</a></li>
               <li><a href='distribuidor-automatico-de-llamadas' title='Distribuidor automático de llamadas'>ACD</a></li>
               <li><a href='respuesta-interactiva-de-llamadas' title='Respuesta de voz Interactiva'>IVR</a></li>
               <li><a href='marcador-predictivo' title='Marcador Predictivo'>Marcador Predictivo</a></li>
               <li><a href='monitoreo-de-llamadas' title='Sistema de monitoreo de llamadas bajo demanda'>Monitoreo de llamadas</a></li>
               <li><a href='grabacion-bajo-demanda' title='Grabación de llamadas bajo demanda'>Grabación de llamadas</a></li>
               <li><a href='integracion-de-telefonia-y-computo' title='Integración de telefonía y computo'>CTI</a></li>
               <li><a href='reportes' title='Reportes'>Reportes</a></li>
               <li><a href='mensajes-sms' title='Mensajes masivos SMS'>Mensajes SMS</a></li>                
           </ul>
       </div>
        ");
    }
    
    function cloud_tags(){  
        echo("
            <div class='col-md-4' style='padding-top: 30px'>
            <h3 class='barNav-tecnology'>Otros servicios</h3>
            <div id='myCanvasContainer' class='cloud_tags'>
                <canvas  height='250' id='myCanvas'>
                    <p>Anything in here will be replaced on browsers that support the canvas element</p>
                </canvas>
            </div>
            <div id='tags'>
                <ul>
                    <li><a href='tecnologia' target='_blank'>Renta tecnolog&iacute;a</a></li>
             <!--<li><a href='index.php?ctl='>Programas de lealtad y retenci&oacute;n de clientes</a></li>-->
                    <li><a href='comunidades'>Comunidades</a></li>
                    <li><a href='consultoria-y-asesoria'>Consultor&iacute;a</a></li>
                    <li><a href='atencion-a-clientes'>Atenci&oacute;n a clientes</a></li>
                    <li><a href='capacitacion-y-mejora-continua'>Capacitaci&oacute;n y mejora continua</a></li>
                    <li><a href='estudios-de-mercado'>Estudios de mercado</a></li>
                    <li><a href='generacion-de-leads'>Generaci&oacute;n de leads</a></li>
                    <li><a href='remainder'>Remainder</a></li>
                    <li><a href='cobranza'>Cobranza</a></li>
                    <li><a href='telemarketing' style='font-size:60px;'>Telemarketing</a></li>
                    <li><a href='backoffice'>Backoffice</a></li>
                </ul>
            </div>
        </div>
        ");
    }
    
    function testimoniales (){
        echo("
            
            <h3>TESTIMONIAL</h3>
            <p><strong>SEGUROS</strong></p>
            
            <p id='typewriter' style='height: 80px; font-size:13px;'>Soy empresario cuento con una firma de consultoría en seguros. Las herramientas proporcionadas por <strong class='emphasis-2'>UNO CSB </strong> nos permiten ser mejores y enfocar nuestro trabajo. Gracias por la gran labor y por su apoyo .</p>
        ");                                                                                                                    
    }            
 ?>