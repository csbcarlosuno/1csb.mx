<?php
ob_start();
?>   
<?php include "functions/funciones.php"?>
<div class="container">
    <div class="col-md-12">
        <div class="col-md-9 alineacion">

            <h2 style="margin-bottom: 35px;">Temas de inter&eacute;s de la industria.</h2>

             <?php foreach ($params['noticias'] as $noticias) : ?>
            <div class="topics">		

                <div class="contentImg-topics">
                    <div class="imgTopics"></div>
                </div>	               
                <div class="contentText-topics">                                        
                    
                        <h3><?php echo $noticias['titulo'] ?></h3>
                        <p><?php echo substr($noticias['texto'], 0, 150) ?></p>
                        <div class="clearfix"></div>
                        <span><a href="<?php echo $noticias['url'] ?>" target="_blanck"><?php echo $noticias['fuente'] ?></a></span>
                        <span class="maia-button">                           
                            <a href="index.php?ctl=noticias-detalle&id=<?php  echo $noticias['id'] ?>" target="_blank">Más información</a></span>                        
                </div>	                
            </div>
            <?php endforeach; ?>
        </div>
        
        <div class="col-md-3">
            <a href="http://www.gimpsaelectrical.com/" title="Gimpsa electrical" target="_blanck">
            <img src="imgs/anuncio-gimpsa-electrical.jpg" class="img-responsive" style="margin-top: 100px;" />
            </a>
        </div>
        <div class="col-md-3">
            <a href="http://www.gimpsa.com/inicio" title="Gimpsa" target="_blanck">
            <img src="imgs/anuncio-gimpsa.jpg" class="img-responsive" style="margin-top: 20px;" />
            </a>
        </div>
        <div class="col-md-3">
            <div class="form-quick" id="pushButtomForm" style="margin-top: 35px;"></div>        
            <?php  formulario__rapido('Noticias', 'none'); ?>
            
            <script type="text/javascript">
            $(document).ready(function(){
                $("#pushButtomForm").click(function(){
                $(".showFormQuitly").slideToggle( "slow" );
                });
            });                             
        </script>
        </div>
        
    </div>
</div>

<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php' ?>

