<?php ob_start(); ?> 
<?php include 'functions/funciones.php'; ?>

    <link rel="stylesheet" type="text/css" href="css/main.css"/>
    <script type="text/javascript" src="js/vendor/jquery.min.js"></script>
    <script type="text/javascript" src="libs/slideandfade/jquery.slideandfade.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>    

<body   onload='$("#typewriter").typewriter(); return false;'>    
<div class="container">

    <div class="display-screen-description col-md-12" id="display-screen1">
            <div class="displayBox" id="displayBox1_0">
                <div class="imagen-atencion-al-cliente"></div>
                <div class="fragment" id="a" style="font-size:24px; left:50px;"><a class="a1">Solución de incidencias</a> </div>
                <div class="fragment" id="b" style=" font-size:12px; top:60px; left:90px;"><strong><a class="b1">Recepción de pagos</a></strong></div>
                <div class="fragment" id="c" style=" font-size:44px; top:70px; left:350px; "><a class="c1">Atención al cliente</a></div>
                <div class="fragment" id="d" style="font-size: 34px; top: 118px; left: 10px;" ><a class="d1">Gestión de sugerencias</a></div>
                <div class="fragment" id="e" style=" top:28px; left: 530px;"><a class="e1">Información del producto</a></div>
                <div class="fragment" id="f" style=" top: 220px; left: 120px;"><a class="f1">Reservaciones</a></div>
                <div class="fragment" id="g" style="top: 150px; left: 300px;"><a class="g1">Confirmación de citas</a></div>
                <div class="fragment" id="h" style=" top: 183px; left: 100px;"><a class="h1">Campañas promocionales </a></div>
                <div class="fragment" id="i" style=" top: 90px; left: 40px;"><a class="i1">Gestión de reclamaciones</a></div>
                <div class="fragment" id="j" style=" top: 100; left:430px;"><a class="j1">Información de servicios</a></div>
                <div class="fragment" id="k" style="top:110px; left: 400px;"><a class="k1">Atención de llamadas de bienvenida</a></div>
                <div class="fragment" id="l" style=" top: 160px; left: 560px;"><a class="l1">Toma de ordenes</a></div>
            </div>
    </div>
        
        <div class="col-md-12">
            <div class="col-md-8 seccion-clientes">
                <h1 class="header-atencion-al-cliente"></h1>              
            <h2>Atenci&oacute;n al cliente</h2>
            
            <p>Con el fin de elevar la eficiencia operativa de los agentes y brindar altos niveles de <span class='emphasis-2'>atención al cliente </span> de manera sencilla y rápida, al menor costo de inversión, proveemos el mejor seguimiento en línea desde nuestras instalaciones. Por otra parte damos un seguimiento de principio a fin en todos los procesos que involucra a nuestros clientes. </p>
            
                <ul class="camera_effected" style="padding: 0;">
                    <li style="background: none;"><strong>Modelo propio que genera la mejor calidad en la atención a clientes:</strong></li>
                    <li>Desarrollamos operaciones garantizando el cumplimiento de los procesos de nuestros clientes de inicio a fin «FULLFILMENT»</li>
                    <li>Desarrollar el sentimiento de lealtad en los clientes, a través de la implementación de iniciativas de prevención, retención y recuperación.</li>
                    <li>Blindamos nuestras campañas a través de áreas de calidad y procesos de grabación (99% de grabaciones)</li>
                    <li>Log&iacute;stica requerida según el proceso</li>
                </ul>                                 
            
            <p style='margin-top: 30px;'><strong>Implementamos operaciones garantizando el cumplimiento de los procesos desarrollados por nuestros socios de negocios de principio a fin</strong></p>
            
            <!-- Back Office
Procesos automatizados de back office diseñados especialmente para gestionar grandes volúmenes y actividades rutinarias en su empresa.
Tamaño del texto
A+
a-
Para conseguir mejores resultados en los procesos empresariales es preciso controlar el estado de su Back Office y tener un aliado de confianzaque conozca el sector de negocio de modo amplio y estratégico.

La solución de Back Office tiene como objetivo facilitarle las tareas de apoyo y "retaguardia" de su empresa, automatizando los procesos de gran volumen, actividades rutinarias y repetitivas.

Garantizamos el control del proceso de Back Office, la productividad y un alto nivel de calidad de todos los procedimientos. Con ello dispondrá de un instrumento esencial para la administración de susprocesos y niveles de servicio (SLAs).-->
            
            </div>
            
            <div class="col-md-4">
                <!--<h2 class="header-form-adaptative">Contácta con nosotros</h2>            -->
            <?php formulario__rapido('atencion a clientes', 'block'); ?>
            </div>

            <div class="col-md-4 testimonial">
                <script type="text/javascript" src="js/typewriter.js"></script>
                <?php testimoniales(); ?>
            </div>
            
        </div>    

</div>
 <?php $contenido = ob_get_clean(); ?>

 <?php include 'layout.php' ?>
