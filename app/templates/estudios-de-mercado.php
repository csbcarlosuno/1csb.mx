<?php 
 ob_start();
 include 'functions/funciones.php';
?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-8 alineacion">
            
            <img src="imgs/estudios-de-mercado.jpg" class="img-responsive img-decorative-sub" />
            <h1>Estudios de mercado</h1>
            
            <p><strong class="emphasis-2">UNO CSB </strong> tiene la capacidad de generar alianzas de negocios con empresas dedicadas a la investigaci&oacute;n de mercado, debido a la versatilidad tanto de su  tecnología como de la experiencias con su staff. <br/><br/>
                        <strong class="emphasis-2"> UNO CSB </strong> colabora dentro del proceso de la investigación con la creación y generación de las encuestas telefónicas, mismas que van desde temas de  satisfacción y productos hasta políticas y de servicio.</p>
            
            <p style="margin-top: 40px;"><strong>Nuestras prioridades son:</strong></p>

                <ul class="style-vineta" style="margin-bottom: 40px;">
                    <li style="width: 90%">Satisfacción de clientes.</li>
                    <li style="width: 90%">Comportamiento del mercado.</li>
                    <li style="width: 90%">Sistemas de grabación 99% del estudio.</li>
                    <li style="width: 90%">Consulta de avances en línea.</li>
                    <li style="width: 90%">Alternativas de monitoreo remoto</li>
                    <li style="width: 90%">Contamos con la capacidad para el desarrollo y  montaje de cuestionarios</li>
                    <li style="width: 90%">Integramos el desarrollo con CATI</li>
                </ul>                                
            
            <p class="bloques-clientes"><strong>Dentro de nuestros socios de negocio se encuentran:</strong></p>
                <img src="imgs/bloques-clientes.jpg" class="img-responsive" style="margin: 0 auto;" />
                
        </div>
        
        <div class="col-md-4">
            <div style="background-color: #FFFBFA; border-radius: 10px; border:  #FADEDF solid 1px;margin-bottom: -20px; margin-top: 10px;">   
                <img src="imgs/llamenos.png" class="img-responsive">
            </div>
        </div>
        
        <div class="col-md-4">            
            <!--<h2 class="header-form-adaptative">Contácta con nosotros</h2>-->
            <?php formulario__rapido('Estudios de mercado', 'block'); ?>
        </div>
        
        <?php barnav_tecnology('col-md-4');?>        
</div>



<?php
$contenido = ob_get_clean();
include 'layout.php';
?>