<?php include "functions/funciones.php"?>
<?php ob_start();?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-8 alineacion">
            <img src="imgs/nosotros.jpg" class="img-responsive img-decorative-sub" />
            <h1>Quiénes Somos</h1> 
            <p><strong class='emphasis-2'>UNO Contact Solutions Bureau</strong> surge de la consolidación del <strong class='emphasis-2'> Grupo IMPSA (GIMPSA), </strong> que tras experimentar un proceso de expansión y crecimiento decide diversificar su presencia en el mercado nacional.</p>
            <p>Somos una empresa dedicada a proveer servicios de contacto a sus socios de negocios bajo diversos esquemas y soluciones, todas hechas a la medida de las necesidades de cada uno de ellos.</p>            
            
            <h2>Misión</h2>
            <p>Brindar servicios integrales de alto nivel tendientes a cubrir las necesidades y altas expectativas de cada uno de nuestros Socios de Negocios, desarrollando soluciones innovadoras de comunicación y estrategias comerciales a través de una plataforma multicanal, operando con personal altamente especializado y tecnología de vanguardia.</p>
            
            <h2>Visión</h2>
            <p>Ofrecer soluciones integrales que vayan más allá de la oferta convencional de un Contact Center, ayudando a nuestros socios comerciales a implementar estrategias efectivas para establecer un contacto valioso y oportuno con sus clientes.</p>
            
            <h2>¿Por qué con UNO?</h2>
            <p><span class="emphasis-2">Por nuestro capital humano:</span> <strong class="emphasis-2"> UNO CSB </strong> cuenta con un equipo de profesionales expertos en el mapeo, diseño, implementación y optimización de operaciones, logrando soluciones innovadoras y vanguardistas en Contact Centers, preocupados siempre por las necesidades de nuestros clientes.</p>
            <p><span class="emphasis-2">Por nuestra estrategia:</span> <strong class="emphasis-2">UNO CSB </strong>estudia cada caso en particular y compone estrategias a la medida de las necesidades específicas de nuestros socios de negocios  mediante un proceso de diagnóstico exhaustivo.</p>
            <p>Identificamos  las áreas de oportunidad y de acción que alcanzarán los mejores resultados para cada uno. Con base en esta información <strong class="emphasis-2"> UNO CSB </strong> desarrolla estrategias de comunicación perfectamente direccionadas y con un alto índice de efectividad.</p>
            <p><span class="emphasis-2">Por nuestra tecnología:</span><strong class="emphasis-2"> UNO CSB </strong>cuenta con tecnología IN HOUSE, combinada con tecnologías externas de última generación para crear una plataforma robusta, que permite responder de forma oportuna a las necesidades más sofisticadas y complejas del mercado.</p>
        </div>
        
         <div class="col-md-4">            
             <!--<h2 class="header-form-adaptative">Contácta con nosotros</h2>-->
            <?php formulario__rapido('atencion a clientes', 'block'); ?>
        </div>
        
        <div class="col-md-4">
            <div style="background-color: #FFFBFA; border-radius: 10px; border:  #FADEDF solid 1px;margin-bottom: -20px; margin-top: 40px;" />   
                <img src="imgs/llamenos.png" class="img-responsive">
            </div>
        </div>
        
        <div class="nube-etiquetas-no-visible">
        <?php  cloud_tags();  ?>
        </div>
    </div>    
</div>
<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>