<?php ob_start();?>
<?php include "functions/funciones.php"?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-9 alineacion">
            <img src="imgs/tecnologias/grabacion-bajo-demanda.jpg" class="img-responsive img-decorative" />
            <h2 class="encabezado-tecnologias">Grabación de llamadas</h2>            
            <p>Con el objetivo de cumplir con las regulaciones gubernamentales <strong class='emphasis-2'> UNO CSB </strong> ofrece un sistema para grabar aleatoriamente o con base a las reglas de negocio dadas. Ponemos al alcance de nuestros clientes una plataforma de grabación multicanal que permite monitorizar el desempeño de los agentes y llevar y un registro de cada contacto establecido</p>
            <p>El sistema de grabación de llamadas  es parte de la suite de herramientas que <strong class='emphasis-2'> UNO CSB </strong> provee. Para su seguridad las grabaciones de audio se encuentran encriptados.</p>            
            <p>También es posible filtrar, buscar y reproducir, desde cualquier lugar donde se encuentre y a través de una simple interfaz web, el audio de llamadas, e-mails, correos de voz y chats.</p>
            
            
            <ul style="margin-top: 40px; float:left; width: 62%; margin-right: 3%; margin-right: 0; padding-left: 0; list-style-type: none;">
                <li style="margin-bottom:20px;"><strong class="emphasis-2">Características:</strong></li>
                <li style="background: none;"><strong class="emphasis-2">Grabación de Llamadas</strong></li>
                    <ul class="camera_effected">
                        <li>Entrantes y Salientes.</li>
                        <li>Total, a demanda, selectiva.</li>
                        <li>Exportación a archivos MP3 para post-proceso.</li>
                        <li>Aplicación de filtro, búsqueda y reproducción de llamadas grabadas.</li>
                    </ul>
                <li style="background: none; margin-top: 20px;"><strong class="emphasis-2">Grabación de Chats y Mails</strong></li>
                    <ul class="camera_effected">
                        <li>Almacenamiento de Web Chats y e-Mails.</li>
                        <li>Aplicación de filtro, búsqueda y visualización de chats y e-mails.</li>
                    </ul>
                <li style="background: none; margin-top: 20px;"><strong class="emphasis-2">Grabación de Correos de Voz:</strong></li>
                    <ul class="camera_effected">
                        <li>Grabación de correos de voz.</li>
                        <li>Aplicación de filtro, búsqueda y reproducción de correos de voz.</li>
                    </ul>                    
                <li style="background: none; margin-top: 20px;"><strong class="emphasis-2">Respaldos:</strong></li>
                    <ul class="camera_effected">
                        <li>Programación de respaldos diarios, semanales y mensuales.</li>
                        <li>Personalización del nombre de los archivos con información de la gestión.</li>
                        <li>Recuperación de respaldos mediante filtros.</li>
                    </ul>                
                <li style="background: none; margin-top: 20px;"><strong class="emphasis-2">Descargas:</strong></li>
                    <ul class="camera_effected">
                        <li>Descarga de grabaciones por lotes.</li>
                        <li>Filtrado y segmentación de grabaciones para descarga.</li>
                    </ul>
            </ul>
            
            <div class="container-ancho-imagen-right" style="margin-top: 55px;">
                     <img src="imgs/tecnologias/grabacion-bajo-demanda-seguras.jpg" class="img-responsive img-decorative-sub" />
            </div>
             <div class='clearfix'></div>                       
        </div>
        
        
        
        <div class="col-md-3">
           
            <div class="form-quick" id="pushButtomForm"></div>
            <?php  formulario__rapido('Grabación de llamadas', 'none'); ?>            
        </div>
        <?php barnav_tecnology('col-md-3') ?>
    </div>
</div>    

<script type="text/javascript">
    $(document).ready(function(){
        $("#pushButtomForm").click(function(){
        $(".showFormQuitly").slideToggle( "slow" );
        });
    });                             
</script>    
<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>