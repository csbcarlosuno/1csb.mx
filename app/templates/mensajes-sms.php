<?php ob_start();?>
<?php include "functions/funciones.php"?>

<div class="container">
    <div class="col-md-12">
        <div class="col-md-9 alineacion">
            <img src="imgs/tecnologias/mensajes-sms.jpg" class="img-responsive img-decorative" />
            <h2 class="encabezado-tecnologias">SMS Masivos, envío masivo de mensajes de voz y encuestas Telefónicas Automatizadas</h2>

            <div style="float:left;">
                <p>Asegure la recepción de sus mensajes con nuestra poderosa herramienta. Es la plataforma más efectiva de comunicación directa con sus clientes y prospectos; en ella es posible crear y administrar múltiples campañas simultáneas en mensajes de una o de dos vías.</p>

                <p><strong class="emphasis-2"><a href="remainder">Reminder</a></strong> es perfecto para generar campañas de lealtad, envío masivo de mensajes de texto, cobranza, promociones, y una amplia gama de servicios y posibilidades.</p>            
            </div>    
            <div class="clearfix"></div>
            
            <div class="container-ancho-imagen-left" style="margin-right: 5%; margin-top: 20px;">
                     <img src="imgs/tecnologias/mensajes-sms-detail.jpg" class="img-responsive img-decorative-sub" />
            </div>              
            
            <ul  class="camera_effected encabezado-tecnologias-subtitulo" style="float:left;">
                <li style="background: none;"><strong class="emphasis-2">Ventajas y beneficios:</strong></li>
                <li>Los mensajes SMS son leídos prácticamente en el momento de su recepción</li>
                <li>Año tras año hay más personas con teléfono celular</li>
                <li>Mensajes bidireccionales o unidireccionales</li>                
                <li>Mejora de la satisfacción del cliente.</li>
                <li>Mensajes personalizados</li>
                <li>A diferencia de las cartas tradicionales, los mensajes SMS cuentan con un alto grado de captación por parte de los destinatarios.</li>
            </ul>                         
            
            <p><strong class="emphasis-2">Mensajes SMS masivos</strong></p>
            <p>El envío de los SMS se realiza de forma sencilla y rápida, con su consiguiente ahorro en costes. Además de representar una ventaja en cuanto accesibilidad ya que el usuario puede volver a consultar la información en caso de que así lo requiera.</p>
            <p><strong class="emphasis-2">Encuestas interactivas</strong></p>
            <p>Decidiéndose por utilizar una campaña de encuestas interactivas contará con la información que se requiere cuando usted lo deseé, lo que le permitirá mejorar su producto o servicio.</p>
            <p><strong class="emphasis-2">Mensajes de voz</strong></p>
            <p>A través de los mensajes de voz es posible establecer un vínculo directo con los clientes</p>
            
                
                
                
        </div>
        
        <div class="col-md-3">
             <!--<img src="imgs/btn-effect-form.jpg" class="img-responsive" id="pushButtomForm" style="cursor:pointer; cursor: hand;" />-->
             <div class="form-quick" id="pushButtomForm"></div>
            <?php  formulario__rapido('Mensajes SMS', 'none'); ?>            
        </div>
        <?php barnav_tecnology('col-md-3') ?>
    </div>
</div>    
<script type="text/javascript">
    $(document).ready(function(){
        $("#pushButtomForm").click(function(){
        $(".showFormQuitly").slideToggle( "slow" );
        });
    });                             
</script>    
<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>