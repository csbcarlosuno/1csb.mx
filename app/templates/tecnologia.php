<?php ob_start();?>
<?php include 'functions/funciones.php' ;?>
<style>
    .btn{ padding: 0 10px; }
</style>
<div class="background-banner"></div>

<div class="container">
    <div class="col-md-12">
                        
        <div class="col-md-12">
            <h1 style="margin-left: 50px;">Tecnologías</h1>          
               <div class="col-md-12">
                   <div class="col-md-4 col-sm-4 col-xs-12 secciones-tecnologia" id="pushButtom1">
                       <h3>Plataformas de Call <br />( licenciamiento )</h3>
                       <span class="logo-inconcert decoracion-iconos"></span>
                       <p>Software y servicios que rentabilizan  la operación de Call center</p>
                       <button class="showButtom1 btn btn-primary pull-right"><a href="plataformas">Más información</a></button>
                   </div>
                   <div class="col-md-4 col-sm-4 col-xs-12 secciones-tecnologia" id="pushButtom2">
                       <h3>ACD <br />(Distribuidor Automático de llamadas)</h3>
                       <span class="icono-acd"></span>
                       <button class="showButtom2 btn btn-primary pull-right"><a href="distribuidor-automatico-de-llamadas">Más información</a></button>
                       <p style="margin-top: -25px;">Distribuye automáticamente las llamadas al mejor agente en el menor tiempo posible, además mide y reporta la productividad de los agentes</p>
                   </div>
                   <div class="col-md-4 col-sm-4 col-xs-12 secciones-tecnologia" id="pushButtom3">
                       <h3>IVR <br />(Respuesta de voz interactiva)</h3>
                       <span class="icono-ivr"></span>
                       <button class="showButtom3 btn btn-primary pull-right"><a href="respuesta-interactiva-de-llamadas">Más información</a></button>
                       <p  style="margin-top: -25px;">Avanzado sistema ideal para atender llamadas repetitivas: consulta de saldos, menús de información,  levantamiento de pedidos, y más.</p>
                   </div>
                       
                   <div class="col-md-4 col-sm-4 col-xs-12 secciones-tecnologia" id="pushButtom4">
                       <h3>Marcador Predictivo</h3>
                       <span class="icono-marcador-predictivo"></span>
                       <button class="showButtom4 btn btn-primary pull-right"><a href="marcador-predictivo">Más información</a></button>
                       <p>Sistema automatizado para marcar lotes de números telefónicos para asignarlo al personal responsable de la campana</p>
                   </div>
                   <div class="col-md-4 col-sm-4 col-xs-12 secciones-tecnologia" id="pushButtom5">
                       <h3>Sistema de monitoreo de llamadas</h3>
                       <span class="icono-monitoreo-de-llamadas"></span>
                       <button class="showButtom5 btn btn-primary pull-right"><a href="monitoreo-de-llamadas">Más información</a></button>
                       <p>Distribuye automáticamente las llamadas al mejor agente en el menor tiempo posible, además mide y reporta la productividad de los agentes</p>
                   </div>
                   <div class="col-md-4 col-sm-4 col-xs-12 secciones-tecnologia" id="pushButtom6">
                       <h3>Grabación de llamadas bajo demanda</h3>
                       <span class="icono-grabacion-bajo-demanda"></span>
                       <button class="showButtom6 btn btn-primary pull-right"><a href="grabacion-bajo-demanda">Más información</a></button>
                       <p>Avanzado sistema ideal para atender llamadas repetitivas: consulta de saldos, menús de información,  levantamiento de pedidos, y más.</p>
                   </div>
                       
                   <div class="col-md-4 col-sm-4 col-xs-12 secciones-tecnologia"id="pushButtom7">
                       <h3>Integración de Telefonía y Cómputo (CTI)</h3>
                       <span class="icono-cti"></span>
                       <button class="showButtom7 btn btn-primary pull-right"><a href="integracion-de-telefonia-y-computo">Más información</a></button>
                       <p>Flexibiliza la información dispuesta desde el teléfono para integrarla a una interfaz gráfica en una computadora.</p>
                   </div>
                   <div class="col-md-4 col-sm-4 col-xs-12 secciones-tecnologia" id="pushButtom8">
                       <h3>Reportes</h3>
                       <span class="icono-reportes"></span>
                       <button class="showButtom8 btn btn-primary pull-right"><a href="reportes">Más información</a></button>
                       <p>Generación de reportes, gráficos, indicadores y estadísticas para el control interno de nuestros socios y clientes</p>
                   </div>
                   <div class="col-md-4 col-sm-4 col-xs-12 secciones-tecnologia" id="pushButtom9" >
                       <h3>Mensajes SMS</h3>                       
                           <span class="icono-sms"></span>                       
                           <button class="showButtom9 btn btn-primary pull-right"><a href="mensajes-sms">Más información</a></button>
                       <p>Envío de mensajes SMS de forma masiva; Mensajes personalizados y todo con una facilidad nunca antes vista</p>
                   </div>
               </div>        
                       <script type="text/javascript">    
                           $(document).ready(function(){
                               $("#pushButtom1").click(function(){
                                $(".showButtom1").slideToggle( "slow" );
                              });
                               
                               $("#pushButtom2").click(function(){
                                $(".showButtom2").slideToggle( "slow" );
                              });
                               
                               $("#pushButtom3").click(function(){
                                $(".showButtom3").slideToggle( "slow" );
                              });
                               
                               $("#pushButtom4").click(function(){
                                $(".showButtom4").slideToggle( "slow" );
                              });
                               
                               $("#pushButtom5").click(function(){
                                $(".showButtom5").slideToggle( "slow" );
                              });
                               
                              $("#pushButtom6").click(function(){
                                $(".showButtom6").slideToggle( "slow" );
                              }); 
                               
                               $("#pushButtom7").click(function(){
                                $(".showButtom7").slideToggle( "slow" );
                              });
                               
                                $("#pushButtom8").click(function(){
                                $(".showButtom8").slideToggle( "slow" );
                              });
                                
                                $("#pushButtom9").click(function(){
                                $(".showButtom9").slideToggle( "slow" );
                              });
                                                            
                           });
                       </script>    
                           
        </div>
               
    </div>    
</div>

<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>