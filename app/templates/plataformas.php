<?php ob_start();?>
<?php include "functions/funciones.php"?>

<body   onload='$("#typewriter").typewriter(); return false;'>
<div class="container">
    <div class="col-md-12">
        <div class="col-md-9 alineacion">
         <img src="imgs/tecnologias/a.jpg" class="img-responsive img-decorative" />
        <h2 class="encabezado-tecnologias">Plataformas</h2>
                    
               <p>Con las plataformas inConcert y Nuxiba rentabilizamos sus negocio racionalizando los procesos. Así usted puede integrar su personal, sus operaciones y sus sistemas a con nuestro contact center y automatizar de forma eficiente los flujos de trabajo. </p>
               <p>Con esta tecnología proveemos de una herramienta de integración muy uniforme y simplificamos incluso los más complejos procesos de negocio. Adicionalmente, con esta infraestructura automatizamos los flujos de trabajo, que hacen posible la realización de los objetivos de nuestros clientes</p>
               <p>Por otra parte la simplificación en los procesos de negocio de su organización logramos reducir costos, mejorar el rendimiento operativo, disminuir tiempos de resolución y brindar una excelente experiencia a sus clientes.</p>

               <h3 class="encabezado-tecnologias-subtitulo">Beneficios</h3>
               <!--<div class="clearfix">-->
                   
                    <ul class="camera_effected" style="float:left; width: 65%; padding-left: 0;">
                        <li>Automatización de procesos.</li>
                        <li>Integración de procesos y sistemas de información </li>
                        <li>Integración con otros sistemas</li>
                        <li>Reducción de costos en desarrollo y mantenimiento de los procesos de negocios</li>
                        <li>Garantizamos el control y consistencia en los procesos de su negocio</li>
                    </ul>
               
                <div class="container-ancho-imagen-right">
                     <img src="imgs/tecnologias/plataformas-adecuadas.jpg" class="img-responsive img-decorative-sub" />
                </div>
                                             
        </div>
        
        <div class="col-md-3 testimonial">
            <script type="text/javascript" src="js/typewriter.js"></script>
            <?php testimoniales();?>
        </div>
        
        <div class="col-md-3">
            <div class="img-para-dispositivos" style="background-color: #FFFBFA; border-radius: 10px; border:  #FADEDF solid 1px; margin: 30px 0;">
                <img src="imgs/llamenos.png" class="img-responsive" />
            </div>
        </div>           
            
        <div class="col-md-3">
            <!--<img src="imgs/btn-effect-form.jpg" class="img-responsive" id="pushButtomForm" style="cursor:pointer; cursor: hand;" />-->
            <div class="form-quick" id="pushButtomForm"></div>

            
            <?php  formulario__rapido('Plataformas', 'none'); ?>            
        </div>
        
            <?php barnav_tecnology('col-md-3') ?>
                        
        
    </div>    
</div>      

<script type="text/javascript">
    $(document).ready(function(){
        $("#pushButtomForm").click(function(){
        $(".showFormQuitly").slideToggle( "slow" );
        });
    });                             
</script>
<?php $contenido = ob_get_clean(); ?>
<?php include 'layout.php'; ?>