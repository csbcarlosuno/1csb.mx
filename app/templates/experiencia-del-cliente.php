<?php
 ob_start();
 include 'functions/funciones.php';
?>

<body   onload='$("#typewriter").typewriter(); return false;'>
<div class="container">
    <div class="col-md-12">
        <div class="col-md-8 alineacion">
            <h2>UNO CSB y la experiencia del cliente:</h2> 
            
            <p>Para <strong class='emphasis-2'>UNO CSB</strong> el valor que damos a nuestros clientes es una interpretación emocional de cómo éstos se sienten cuando se relacionan con la empresa. En muchas ocasiones el cliente ya tiene experiencia en el rubro, los servicios que ofrece un call center benefician en muchos sentidos a las empresas aunque no necesariamente los clientes siempre queden satisfecho, por eso uno de nuestros objetivos es lograr un <span class='emphasis-2'>cambio de enfoque </span> que los haga tomar conciencia de la importancia que tiene trabajar con expertos. </p>
            <p>Comprendemos los aspectos más escondidos que requieren nuestros clientes para su crecimiento por ello nuestro modelo de negocio se basa, también, en mejorar su percepción desde el mismo momento en que deciden trabajar con nosotros. Tratamos siempre de entender realmente al cliente y enfatizar con él porque  como personas lo que recordamos son las emociones, la visión subjetiva de cómo nos han hecho sentir es parte primordial para <span class='emphasis-2'> UNO Call center </span></p>  
            
            <h2 style="text-align: center; margin-top: 50px;">Aumente la satisfacción de sus clientes e incremente sus ventas y utilidades</h2>                        
            
        <div class="col-md-6" style='margin-top: 65px;'>    
            <img src="imgs/experiencia-del-cliente-uno-csb.png" class="img-responsive" />
        </div>    
        
        <div class="col-md-6">
            <ul class="style-vineta">
                <li style="background: none; border-bottom: none; margin: 30px 0;"><strong>Nuestras prioridades son:</strong></li>
                <li style="width: 90%;">Satisfacción de  clientes</li>
                <li style="width: 90%;">Comportamiento del mercado</li>
                <li style="width: 90%;">Sistemas de grabación 99% del estudio</li>
                <li style="width: 90%;">Contamos con la capacidad para el desarrollo y montaje de cuestionarios</li>
                <li style="width: 90%;">Integramos el desarrollo CATI</li>
                <li style="width: 90%;">Consulta de avances en línea</li>
                <li style="width: 90%;">Alternativas de monitoreo remoto</li>
                <li style="width: 90%;">Sistemas de grabación 99% del estudio</li>
            </ul>
        </div>
        
            <div class="col-md-12">
                <img src="imgs/llama-ahora-uno-call-center.jpg" class="img-responsive" style="margin-top: 90px;" />            
            </div>    
                
        </div>
        
        <div class="col-md-4">
            <div class="img-para-dispositivos" style="background-color: #FFFBFA; border-radius: 10px; border:  #FADEDF solid 1px; margin-bottom: 50px; ">
                <img src="imgs/llamenos.png" class="img-responsive" />
            </div>                        
            <?php formulario__rapido('Experiencia del cliente', 'block'); ?>
        </div>
        
        <div class="col-md-4 testimonial">
            <script type="text/javascript" src="js/typewriter.js"></script>
            <?php testimoniales();?>
        </div>
        
        
    </div>
</div>    



<?php
$contenido = ob_get_clean();
include 'layout.php';
?>