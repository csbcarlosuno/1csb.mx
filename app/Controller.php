<?php

class Controller {

    public function inicio() {
        require __DIR__ . '/templates/inicio.php';
    }
    
    public function nosotros(){
        require __DIR__ .'/templates/valores.php';        
    }

    public function contacto() {
        require __DIR__ . '/templates/contacto.php';
    }
    
    public function generacion_de_leads(){
        require __DIR__.'/templates/generacion-de-leads.php';
    }
    
    public function telemarketing(){
        require __DIR__.'/templates/telemarketing.php';
    }
    
    public function remainder(){
        require __DIR__.'/templates/remainder.php';
    }
    
    public function experiencia_del_cliente(){
        require __DIR__.'/templates/experiencia-del-cliente.php';
    }
    
    public function estudios_de_mercado(){
        require __DIR__.'/templates/estudios-de-mercado.php';
    }

   public function tecnologia(){       
                require __DIR__.'/templates/tecnologia.php';                        
    }
    
   public function plataformas(){       
                require __DIR__.'/templates/plataformas.php';                        
    }

   public function acd(){       
                require __DIR__.'/templates/distribuidor-automatico-de-llamadas.php';                  
    }
    
   public function ivr(){
                require __DIR__.'/templates/respuesta-de-voz-interactiva.php';                
    }
    
   public function marcador_predictivo(){                       
                require __DIR__.'/templates/marcador-predictivo.php';                        
    }
    
   public function monitoreo_de_llamadas(){       
                require __DIR__.'/templates/monitoreo-de-llamadas.php';                        
    }
    
   public function grabacion_bajo_demanda(){    
                require __DIR__.'/templates/grabacion-de-llamadas.php';   
   }
    
   public function integracion_de_telefonia_y_computo(){       
                require __DIR__.'/templates/integracion-de-telefonia-y-computo.php';                
    }
    
   public function reportes(){                       
                require __DIR__.'/templates/reportes.php';                        
    }
    
   public function mensajes_sms(){                       
                require __DIR__.'/templates/mensajes-sms.php';                        
    }    
    
    public function cobranza(){
        require __DIR__.'/templates/cobranza.php';
    }

    public function capacitacion_y_mejora_continua(){
        require __DIR__.'/templates/capacitacion-y-mejora-continua.php';
    }

    public function backoffice() {
        require __DIR__ . '/templates/backoffice.php';
    }
    
    public function bases() {
        require __DIR__ . '/templates/bases.php';
    }
    
    public function galerias() {
        require __DIR__ . '/templates/galeria.php';
    }
    
      public function comunidades() {
        require __DIR__ . '/templates/comunidades.php';
    }

    public function consultoria_y_asesoria() {
        require __DIR__ . '/templates/consultoria-y-asesoria.php';
    }

    public function atencion_al_cliente() {        
        require __DIR__ . '/templates/atencion-al-cliente.php';                        
    }
   
    public function exito() {
        require __DIR__ . '/templates/exito.php';
    }                    
    
     public function aportaciones() {
        require __DIR__ . '/templates/aportaciones.php';
    }
    
    public function error() {
        require __DIR__ . '/templates/ajax/error.php';
    }
    
    public function thankyou() {
        require __DIR__ . '/templates/ajax/thankyou.php';
    }                   
    
    public function procesa_contacto(){                
        
        $nombre = $_POST['name'];
        $empresa =$_POST['company'];
        $correo = $_POST['email'];
        $telefono = $_POST['phone'];
        $issue = $_POST['issue'];            
        $comentario = $_POST['message'];
        $oculto = $_POST['oculto'];    
     /*             
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";
       */    
       
         $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,
                     Config::$mvc_bd_clave, Config::$mvc_bd_hostname);
                
        $m->validarDatos($nombre, $empresa, $correo, $telefono, $issue, $comentario, $oculto);
        $m->insertarDatos($nombre, $empresa, $correo, $telefono, $issue, $comentario, $oculto);
    
        if($oculto == 'Pagina de contacto') {
            
            $mensaje = "Formulario de contacto desde sitio web UNO CSB <br/><br/> Nombre:  " . $nombre . " <br/>"
                    ."Empresa: " . $empresa . "<br/>"
                    ."Correo: " . $correo . "<br/>"
                    ."Telefono: " .$telefono . "<br/>   "
                    ."Interes: " . $issue. "<br/>"
                    ."Comentario: " .$comentario. "<br/>"
                    ."Proviene de: " . $oculto. ".";            
                    
        }else{
            $mensaje = "Formulario de contacto desde sitio web UNO CSB <br/><br/>"
                    ."Nombre: " .$nombre. "<br/>"                        
                    ."Correo: ".$correo. "<br/>"                       
                    ."Comentario: " .$comentario. "<br/>"
                    ."Proviene de: " .$oculto. ".";                                
        }
        
		require("PHPMailer-master/class.phpmailer.php");
		$mail = new PHPMailer();
		//$mail->PluginDir = "/";
		$mail->Mailer = "smtp";
		$mail->Host = "mail.1csb.com.mx";
		//$mail->SMTPAuth = true;
		$mail->Username = "contacto@1csb.mx";
		$mail->Password = "Contact187c";
		$mail->From = "contacto@1csb.mx";
		$mail->FromName = $oculto;
		$mail->Timeout=30;
		$mail->Subject = "Contactacion web";		
                                        $mail->AddAddress("jlopez@isb-cc.com","");
		$mail->AddAddress("esanchez@isb-cc.com","");
		$mail->AddAddress("cristobal@isb-cc.com","");
		$mail->AddAddress("renesol@prodigy.net.mx","");
		$mail->AddAddress("cgutierrez@1csb.com.mx","");
		$mail->Body =  $mensaje;
		$mail->AltBody = $mensaje;
		$exito = $mail->Send();

		if(!$exito) {
		//	echo "Problemas enviando correo electrónico a ";
		//	echo "<br/>".$mail->ErrorInfo;
		}
		else 
		{
		header("Location: http://1csb.mx/web/exito");                                         
		} 
				
    }
    
    public function procesa_sugerencias(){
        
        $nombre = $_POST['name'];        
        $correo = $_POST['email'];
        $comentario = $_POST['message'];
        
        $errorurl = "http://1csb.mx/web/error" ;
        $thankyouurl = "http://1csb.mx/web/thankyou" ;
        
        if (empty($comentario)) {
                header( "Location: $errorurl" );
            exit;
        }               
        
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,
                     Config::$mvc_bd_clave, Config::$mvc_bd_hostname);
        
        //$m->validarDatos($nombre, $correo, $comentario);
        $m->insertarDatosSugerencias($nombre,  $correo, $comentario);
        
        
          $mensaje = "Formulario Quejas y sugerencias UNO CSB <br/><br/> Nombre:  " . $nombre . " <br/>"                    
                    ."Correo: " . $correo . "<br/>"                                       
                    ."Comentario: " .$comentario. ".";
          
          
          require("PHPMailer-master/class.phpmailer.php");
		$mail = new PHPMailer();
		//$mail->PluginDir = "/";x
		$mail->Mailer = "smtp";
		$mail->Host = "mail.1csb.com.mx";
		//$mail->SMTPAuth = true;
		$mail->Username = "comunicacion@1csb.mx";
		$mail->Password = "Com1.mx";
		$mail->From = "comunicacion@1csb.mx";
                                    $mail->FromName = "1csb.mx";
		$mail->Timeout=30;
		$mail->Subject = "Quejas y sugerencias";                                                                               		
                               /*  $mail->AddAddress("esanchez@isb-cc.com","");*/
	/*	$mail->AddAddress("renesol@prodigy.net.mx","");*/
                                    $mail->AddAddress("cgutierrez@1csb.com.mx","");
		$mail->Body =  $mensaje;
		$mail->AltBody = $mensaje;
		$exito = $mail->Send();                                                        
                
                
          if($exito){
                            header( "Location: $thankyouurl" );		
                        }              
    }                                
                
    
    /***************Noticias*************************/
     public function noticias_detalle() {
         $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,
                     Config::$mvc_bd_clave, Config::$mvc_bd_hostname);
         
         $id = $_GET['id'];                   
         
         $params = array(
            'noticias' =>  $m -> dameNoticiasID($id),
        );    
        
        require __DIR__ . '/templates/noticias-individuales.php';
        
    }
    
    public function noticias() {               
        
         $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,
                     Config::$mvc_bd_clave, Config::$mvc_bd_hostname);
        
        $params = array(
            'noticias' => $m -> dameNoticias(),
        );         
        
        require __DIR__ . '/templates/noticias.php';
    }     
    
    
    
    /**************Administrador*******************/        
    
    public function procesa_admin(){
        session_start();
        session_destroy();
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);
        
        require __DIR__ . '/templates/functions/procesa-admin.php';
    }        

    public  function sesion_cms(){                                     
        session_start();
        if (!isset($_SESSION['administrador'])) {
             //session_start();
             //session_destroy();
            echo 'Hay problemas con el inicio de sesion, vuelva a intentar <br />';            
            echo '<script>';
            echo "setTimeout(\"location.href='http://1csb.mx/admin'\", 5000)";
            echo '</script>';
        }else{     
             $mensaje = 'Has iniciado sesión correctamente<br />';
             $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);    
             $params = array(
              'noticias' => $m->dameNoticiasCMS(),
                );
             
             $noticasActuales = array(
                 'actuales' => $m->dameNoticias(),
             );
             
              require __DIR__ .'/administrador/inicio.php';                                       
        }  
}

    public  function elimina_notice(){
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,
                     Config::$mvc_bd_clave, Config::$mvc_bd_hostname);
        
        $id_eliminar = $_GET['id'];         
        $m ->eliminaNoticiasID($id_eliminar);
                
    }

public function envia_datos(){
        $errorurl = "http://1csb.mx/web/error" ;
        $autor = $_POST['autor'];
        $titulo = $_POST['titulo'];        
        $contenido = $_POST['contenido'];
        $fuente = $_POST['fuente'];
        $url = $_POST['url'];
        
        if (empty($contenido)) {
                header( "Location: $errorurl" );
            exit;                                    
        }
                
        $texto = str_replace("\n", '<br/>', $contenido);                                                         
        
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,
                     Config::$mvc_bd_clave, Config::$mvc_bd_hostname);
                
        
        $m->insertarNoticias($autor, $titulo,  $texto, $fuente, $url);        
        //$m -> creaXML();
        
         header ("location: http://1csb.mx/web/noticias-de-la-industria");
       
    }

    public  function xml(){                                 
        
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,
                     Config::$mvc_bd_clave, Config::$mvc_bd_hostname);
        
        $m -> creaXML();
                 
        header ("location: http://1csb.mx/web/noticias-de-la-industria");
    }


    /*
  public function news($display) {
                    echo "<div class='feed marquee' style='' .'$display'. '>";
                    $long_descripcion = 100;
                    $num_noticias = 5;
                    $n = 0;
                    echo ("
                        <div class='conteinerNews' style='$display'>");
                            $noticias = simplexml_load_file('http://contactcenters.wordpress.com/feed/');
                            foreach ($noticias as $noticia) {
                                foreach ($noticia as $reg) {
                                    if ($reg->title != NULL && $reg->title != '' && $reg->description != NULL && $reg->description != '' && $n < $num_noticias) {
                                        echo "
                                                                <span class='feed'><a href='" . $reg->link . "' target='_blank' style='margin-top:10px;'>" . $reg->title . "</a></span>";
                                        $n++;
                                    }
                                }
                            }
                        echo "</div>";
                    echo "</div>";
                    
                    require __DIR__ . '/templates/layout.php';
    }
    */            
      
}