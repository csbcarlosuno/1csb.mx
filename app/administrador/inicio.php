<?php ob_start() ?>

<style>
    input, textarea{
        color: #999;
    }
    body{
        background-image: url("imgs/imgFond-admin.png");
        background-position: 0 0px;
        background-repeat: repeat-x;
    }
    label{
        width: 123px;
        text-align: right;
        float: left;
        padding: 5px 15px 0 0;
    }
    
    input[type="text"]{
        width: 550px;
    }
    
    textarea{
        width: 550px;
        height: 220px;
    }
    form{
        width: 65%;
        float:left;
    }

</style>

<div class="container" style="border-top: 1px dashed #CCCCCC;">
    <div class="col-md-11">
        <h1>Administrador</h1>
        <?php echo $mensaje ?>
        
        <p><a href="index.php?ctl=news">Habilitar feeds RSS</a></p>
    </div>
    <div class="col-md-1">
        <input class="btn btn btn-danger" type="button" value="Salir" style="margin-top:44px;" onclick="location='admin'" />
    </div>
</div>    

<div class="clearfix"></div>
<?php include 'menuCMS.php'; ?>
<div class="container">              
        
    <div class="clearfix"></div>         
    <div>            
        <h3>Temas de interés de la industria</h3>
        <p><span>Ingresa los contenidos de la industria que sean de interés para los usuarios</span></p>
        <div>El contenido aquí visible se visualiza en la sección <a href='http://1csb.mx/web/noticias-de-la-industria'> Noticias </a> del sitio</div>
        
        <div class="well well-sm" style='padding-top: 60px;'>                        
            <form method="post" action="index.php?ctl=envia-datos" name='Contacto' id="login">
                <?php foreach ($params['noticias'] as $noticias) : ?>                                
                
                <div class="form-group">
                    <label for="autor">Autor:</label>                    
                        <input type="text" required name="autor" id="title" value="<?php echo $noticias['autor']  ?>"/>                    
                </div>    
                
                <div class="form-group">
                    <label for="titulo">Titulo:</label>                    
                        <input type="text" required name="titulo" id="title" value="<?php echo $noticias['titulo']  ?>"/>                    
                </div>    

                <div class="form-group">
                    <label for="contenido">Contenido:</label>                    
                        <textarea required name="contenido" id="content"><?php  echo $noticias['texto'] ?></textarea>                    
                </div>

                <div class="form-group">
                        <label for="fuente">Fuente:</label>                        
                            <input type="text" required name="fuente" id="source" value="<?php echo $noticias['fuente']?>"/>                        
                </div>

                <div class="form-group">
                    <label for="url">Url:</label>                    
                        <input type="text" required name="url" id="url" value="<?php echo $noticias['url'] ?>"/>                    
                </div>

                <input type="submit"  class="btn btn-primary pull-right" id="btnContactUs" value='Actualiza' style='margin-right:55px;' /><!-- onClick="validarDatos();" -->
                <?php  endforeach; ?>                
            </form>            
            
            <h3 style="margin-top:0;">Noticias actuales</h3>
            <p style="color:#8F8F8F; font-size: 11px;">*** Selecciona la nota que deses eliminar ***</p>
            <?php foreach ($noticasActuales['actuales'] as $noticias) : ?>
                <p><?php echo $noticias['titulo']  ?> <input type="checkbox" id="delete" onclick="a('eliminaID',<?php echo $noticias['id'] ?>);" value="<?php echo $noticias['id'] ?>" name="<?php echo $noticias['id'] ?>"></p>
            <?php endforeach; ?>
            
            <div><a href="index.php?ctl=xml">Crea xml</a></div>            <!-- onclick="return false href="index.php?ctl=xml"-->
            
            <script type="text/javascript">
               function a(valor0,valor){
                   var a=valor0;
                   var b=valor;
                   
                   $.ajax({
                    url: 'index.php',
                    data: {id: b, ctl:a},
                    type: 'GET',                    
                    success: function(json) {
                        
                        alert('Noticia Eliminada');                        
                    },
                    error: function(jqXHR, status, error) {
                        alert('Disculpe, existió un problema');
                    },
                    complete: function(jqXHR, status) {
                        alert('Petición realizada');
                    }
                });


                   
               }
                    $(function() {
                        ('#delete').change(function(event){
                            
                        })
                    });
            </script>
            
            <div class='clearfix'></div>
        </div>    
    </div>  
</div>

<div id="pleca" style='margin-top: 60px;'></div>

 <?php $contenido = ob_get_clean() ?>

 <?php include 'layoutAdmin.php' ?>