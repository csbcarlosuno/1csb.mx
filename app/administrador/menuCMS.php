<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style>
    .navbar-nav li{ border-right: #C2C2C2 1px solid;}
    .navbar-default .navbar-nav>li>a {color:#525252;}
</style>

 <nav class="navbar navbar-default" role="navigation"  id='cabecera' style="background-color: #CCC; border: 2px #C1C1CB solid">
         <div class="container-fluid">
             <!-- Conmutación de la navegación para dispositivos mobiles -->
             <div class="navbar-header">
                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                 </button>
                 <!--<a class="navbar-brand" href="#">Call center UNO</a>-->
             </div>

             <!-- Collect the nav links, forms, and other content for toggling -->
             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                 <ul class="nav navbar-nav">
                     <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Inicio<b class="caret"></b></a><span>Conócenos</span>
                         <ul class="dropdown-menu">                             
                             <li class="pagina-no-visible"><a name="galeria"  onclick="return false">Galer&iacute;a</a></li>
                         </ul>
                     </li>
                     <li class="dropdown">
                         <a name="administrador/valores"  onclick="return false">Nosotros<!--<b class="caret"></b>--></a><span>Qui&eacute;nes somos</span>                        
                     </li>
                     <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Servicios<b class="caret"></b></a><span>Lo mejor que hacemos</span>
                         <ul class="dropdown-menu">
                             <li class="dropdown-submenu">
                                 <a href="#">Call Center</a>
                                 <ul class="dropdown-menu">
                                     <li><a name="administrador/telemarketing" onclick="return false">Telemarketing</a></li>
                                 </ul>
                             </li>
                             <li class="dropdown-submenu">
                                 <a href="#">Contact Center</a>
                                 <ul class="dropdown-menu">
                                     <li><a name="administrador/generacion-de-leads" onclick="return false"s>Generación de leads</a></li>
                                     <li><a name="administrador/cobranza" onclick="return false">Cobranza</a></li>
                                     <li><a name="administrador/atencion-a-clientes" onclick="return false">Atenci&oacute;n al cliente</a></li>
                                    <li><a name="administrador/remainder" onclick="return false">Remainder</a></li>
                                    <li><a name="administrador/experiencia-del-cliente" onclick="return false">Experiencia del cliente</a></li>
                                    <li><a name="administrador/estudios-de-mercado" onclick="return false">Estudios de mercado</a></li>
                                    <li><a name="administrador/capacitacion-y-mejora-continua" onclick="return false">Capacitaci&oacute;n y mejora continua</a></li>                         
                                 </ul>
                             </li>
                             <li class="dropdown-submenu">
                                 <a href="#">Consultor&iacute;a</a>
                                 <ul class="dropdown-menu">
                                     <li><a name="administrador/consultoria-y-asesoria" onclick="return false">Consultor&iacute;a y asesor&iacute;a</a></li>
                                     <li><a name="administrador/backoffice" onclick="return false">Backoffice</a></li>
                                 </ul>
                             </li>                  
                         </ul>
                     </li>                     
                     <li><a name="administrador/comunidades" onclick="return false">Comunidades</a><span>Subcontrataciones</span></li>
                     <li><a name="administrador/tecnologia" onclick="return false">Tecnolog&iacute;a</a><span>Sistemas avanzados</span></li>
                     <li><a name="administrador/contacto" onclick="return false">Contacto</a><span>Con gusto te atenderemos</span></li>
                     <li style='background-color: #C2C2C2;'><a href="index.php?ctl=administrador">Noticias</a><span>Temas de inter&eacute;s</span></li>
                 </ul>
                 
             </div><!-- /.navbar-collapse -->
         </div><!-- /.container-fluid -->
     </nav>