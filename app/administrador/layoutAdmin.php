<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html lang="en">
     <head>         
         <meta name="robots" content="noindex, nofollow" />
         <meta http-equiv="content-type" content="text/html; charset=utf-8" />
         <title>: Content Manager System </title>
         <link rel="stylesheet" href="dist/css/bootstrap.min.css" />
         <link rel="stylesheet" href="<?php echo 'css/' . Config::$mvc_vis_css; ?>" />
         <script type='text/javascript' src='scripts/jquery-1.7.2.min.js'></script>
     </head>   
     
     <body>
         <?php echo $contenido ?>
     </body>
     
</html>    