<?php

 class Model
 {
     protected $conexion;

     public function __construct($dbname,$dbuser,$dbpass,$dbhost)
     {
       $mvc_bd_conexion = mysql_connect($dbhost, $dbuser, $dbpass);       

       if (!$mvc_bd_conexion) {
           die('No ha sido posible realizar la conexión con la base de datos: ' . mysql_error());
       }
       mysql_select_db($dbname, $mvc_bd_conexion);

       mysql_set_charset('utf8');

       $this->conexion = $mvc_bd_conexion;
     }	 
    
    public function bd_conexion()
    {

    }
    
    public function valida_ingreso(){
        
    }
    
    public function dameInformacion_banner(){
        $sql = "select * from banner";

         $result = mysql_query($sql, $this->conexion);

         $resultados = array();
         while ($row = mysql_fetch_assoc($result))
         {
             $resultados[] = $row;                                 
         }

         return $resultados;
    }
    
     
     public function insertarDatos($name, $company, $mail, $phone, $issue, $message, $proveniente)
     {

         $sql = "insert into contactos (nombre, empresa, correo, telefono, asunto, mensaje, proveniente) values (
             '" . $name . "','" . $company . "','" . $mail . "','" . $phone . "','" . $issue . "','" . $message . "', '" .$proveniente. "'
            )";                 
         
         $result = mysql_query($sql, $this->conexion);
         return $result;
         
     }
     
     public function insertarDatosSugerencias($name, $mail, $message)
     {

         $sql = "insert into interno (nombre, correo, mensaje) values (
             '" . $name . "','" . $mail . "','" . $message . "'
            )";                 
         
         $result = mysql_query($sql, $this->conexion);
         return $result;
         
     }

     public function validarDatos($name, $company, $mail, $phone, $issue, $message, $proveniente)
     {
         return (is_string($name) &
                 is_string($company) &
                 is_string($mail) &
                 is_string($phone) &
                 is_string($issue) &
                 is_string($message) &
                 is_string($proveniente));
                          
     }
     
     public function insertarNoticias($autor, $title,$content,$source,$url){                  
         
         $sql = "insert into noticias (autor, titulo, texto, fuente, url) values (
             '" . $autor . "','" . $title . "','" . $content . "','" . $source . "','" . $url . "'
            )";                 
         
         $result = mysql_query($sql, $this->conexion);
         return $result;
     }
     
     public function dameNoticias(){                  
         
         //$sql = "select * from noticias order by id DESC limit 1";
         $sql = "select * from noticias";
         
         $result = mysql_query($sql, $this->conexion);
         
         $noticias = array();
         while ($row = mysql_fetch_assoc($result))
         {
             $noticias[] = $row;
         }

         return $noticias;                  
     }
     
     public function dameNoticiasCMS(){
         $sql = "select * from noticias order by id DESC limit 1";
         
         $result = mysql_query($sql, $this->conexion);
         
         $noticias = array();
         while ($row = mysql_fetch_assoc($result))
         {
             $noticias[] = $row;
         }

         return $noticias;
     }

     public function dameNoticiasID($id){
         //$sql = "select * from noticias order by id DESC limit 1";
         $ids=  $id;
         $sql = "select * from noticias where id='".$ids."'";
         
         $result = mysql_query($sql, $this->conexion);
         
         $noticias = array();
         while ($row = mysql_fetch_assoc($result))
         {
             $noticias[] = $row;
         }

         return $noticias;                  
     }
     
     public function eliminaNoticiasID($id_eliminar){         
         $sql = "delete from noticias where id ='".$id_eliminar."'";
          $result = mysql_query($sql, $this->conexion);
                  
     }
     
     public function creaXML(){                 
         
         $xml          = '<?xml version="1.0" encoding="UTF-8"?>';
         
         $root_element = "noticias";
         
         $sql = "select * from noticias";
         //$result = mysql_query($sql, $this->conexion);                  
           $result = mysql_query($sql);   
         
         if (!$result) {
                die('Invalid query: ' . mysql_error());                
         }
                  
         $xml .= "<root>"; 
         
            if (mysql_num_rows($result) > 0) {                
            while ($result_array = mysql_fetch_assoc($result)) {
                $xml .= "<" . $root_element . ">";

                //loop through each key,value pair in row
                foreach ($result_array as $key => $value) {
                    //$key holds the table column name
                    $xml .= "<$key>";

                    //embed the SQL data in a CDATA element to avoid XML entity issues
                    //           $xml .= "<![CDATA[$value]]>";
                    $xml .= $value;

                    //and close the element
                    $xml .= "</$key>";
                }

                $xml.="</" . $root_element . ">";
            }
        } 
                
        //close the root element
        //$xml .= "</$root_element>";
        $xml .= "</root>";                         
        
        
            //nombre del archivo
            $nombre = 'xml/news.xml';
            $archivo = fopen($nombre, "w+");     
            fwrite($archivo, $xml);        
            fclose($archivo);           

     }

 }